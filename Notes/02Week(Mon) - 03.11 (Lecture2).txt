그래픽스 파이프라인의 각 단계는 독립적인 일을 수행한다
무슨 일을 할지 미리 지정을 해야한다.

지정이 되면, 그 일만 하게 된다.
일종의 State Machine처럼 동작하게 된다.

그래픽스 파이프라인은 지정된 동작만 해야한다. 
★ 한번 설정한 State는 렌더링 중간에 바뀌지 않는다. ★ 4p (렌더링 도중에 데이터가 바뀌는 것을 허용하지 않음!)


# OpenGL 렌더링 구조
- 병렬화 (성능 극대화를 위해)
- 고유의 Data 형식을 가짐 (효율적인 렌더링을 위해)

뭔가 그릴려고 할 때 가장 먼저 준비 해야 할 것 : Vertex (그래픽스 파이프라인은 지정된 동작만 하기 때문에, Vertex를 넘겨주지 않으면 작동을 하지 않는다)

Vertex Shader의 출력은 화면에 나오려면 무조건 -1, 1 사이어야 한다.

gpu 메모리와 cpu 메모리가 분리 되어 있는이유. Interrupt를 걸지 않고 효율적으로 수행하기 위해 분리되어 있음.

따라서 cpu memory에서 gpu memory에 넣는 과정이 필요하다.
cpu 메모리상에 좌표를 아무리 넣어봤자. gpu 메모리상으로 옮겨주지 않으면 효율적으로 동작하지 않는다.

# GPU에 데이터를 올리기위한 3가지 과정

# VBO를 생성한다.
- glGenBuffers(1, &VBO)
성공 했을 경우 0보다 큰 값을 return하게 된다.  

# 실제 OpenGL에서 작업할 대상을 골라주는 것.
-glBindBuffer(GL_ARRAY_BUFFER, VBO);
생성된 VBO를 GL_ARRAY_BUFFER라는 형태로 사용할 것이다.

★ GL_ARRAY_BUFFER는 OpenGL에 단 하나 존재하고, 일종의 작업대라 생각하면 편함 ★

# Bind된 VBO에 데이터를 할당
- glBufferData(GLenum target, GLsizeiptr size, const GLvoid *data, GLenum usage);

예시)
GLuint VBO;
glGenBuffers(1, &VBO);
// 0 ? VBO

glBindBuffer(GL_ARRAY_BUFFER, VBO);
glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);


마지막 두 줄 보면, VBO가 GL_ARRAY_BUFFER에 있기 때문에 glBufferData()에서 GL_ARRAY_BUFFER로 사용한다.


Tip) 동적 할당된 값을 sizeof하게되면, 포인터의 크기가 리턴된다.


----------------------------------------------

# OpenGL 데이터 사용
1. 일단 BIND 해야한다.

2. 몇개씩 뜯어서 읽을 것인가 알려줌.

ARRAY란것만 알고, 그 ARRAY에서 몇개씩 떼서 무엇으로 구성해야 하는지는 GPU는 알지 못한다.
실제로 의도했던 size가 무엇인지 알려주어야 한다.
-> glVertexAttribPointer
(Draw시 데이터를 읽어갈 단위의 크기 및 시작점 설정)

int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
glEnableVertexAttribArray(attribPosition);
glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

3개씩 끊어서 읽을것이고, 각각은 FLOAT형이다.
array에는 총 18개의 float으로 이루어져있다. (3, GL_FLOAT) = float을 3개씩 읽어라,
다음 거 읽을 때는 sizeof(float) *3 만큼 뛰어넘고 읽어라. (읽을 시점) ==>STRIDE (건너 뛸 사이즈를 알려줌)

근데 0이라고 쓰면, OpenGL이 알아서 3개씩 읽으라고 이해한다.
근데 만약, Vertex 정보에 x, y, z, u, v라는 5개의 정보가 들어갔을 때, 0을 써버리면 엉뚱한 결과값이 나와버린다.


glDrawArrays(GL_TRIANGLES, 0, 6);
Primitive Assembly 단계에서 GL_TRIANGLES로 형태를 구성하라고 명령 함
그리고 Vertex 를 6개로 구성하라는 말.

-----------

int attribPosition = glGetAttribLocation(m_SolidRectShader, "a_Position");
glEnableVertexAttribArray(attribPosition);
glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
glDrawArrays(GL_TRIANGLES, 0, 6);
거꾸로 올라가게 되면 6개의 vertex이고, 윗 줄에 3개의 float으로 구성되어 있다 = 18개의 float값으로 구성되어있구나!
