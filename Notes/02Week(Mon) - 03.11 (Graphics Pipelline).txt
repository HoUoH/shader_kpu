# 그래픽스 파이프라인

- 파이프라인
예시) https://ko.wikipedia.org/wiki/%ED%8C%8C%EC%9D%B4%ED%94%84%EB%9D%BC%EC%9D%B8_(%EC%BB%B4%ED%93%A8%ED%8C%85)
: 한마디로, 효율적으로 수행하기 위해서 현 단계(Stage)의 출력이 다음 단계의 입력으로 이어지는 형태로 연결된 구조

# Vertex Transform
Vertex Transform의 입력 : vertices
Vertex Transform의 출력 : vertices
버텍스 트랜스폼에서는 1개의 vertex가 입력이 되면, vertex transform연산이 1번 수행된다.
버텍스당 연산이라고 한다. (입력이 버텍스 하나면, 출력도 버텍스 하나다)

# Primitive Assembly
입력된 버텍스의 개수 4개 : Points 4개, Lines (4개의 점으로 만들 수 있는 선들의 조합), Triangle (이것도 조합)
- 버텍스의 정보를 가지고 요소를 구현한다 (실체화)

# Rasterization and Interpolation
점과 점 사이의 픽셀들을 이어주지 않으면 선이 그려지지않는다.
rasterize : 사이를 채운다
interpolate : 보간한다
- 입력 전 : Points인지, Lines인지 Triangles인지 모르는데, Rasterizatoin and Interpolation을 거치게되면
어떤 형태인지 알 수 있게 된다.

가공하기 전의 픽셀 : Fragment Shader (OpenGL) =  PixelShader (DirectX) 이라고 부른다.

# Fragment Operation
- 광원 효과 같은것을 추가해서 Frame Buffer에 옮긴다.

# Frame Buffer
- 모니터가 Frame Buffer에 있는 내용들을 1초에 60번씩 가져간다.


# Vertex Transform이 Vertex Shader로 변화하면서 애니메이션 같은 기능들을 손쉽게 구현할수 있게 됨


★ 시험 ★ Lecture1의 25페이지 파이프라인 시험에 나옴 (각 단계에서 무엇을 할 것인지)
- 텍스쳐를 주고, 순차적으로 배치하시오( 시험문제 )