#include "stdafx.h"
#include "Renderer.h"
#include "LoadPng.h"
#include <Windows.h>
#include <cstdlib>
#include <cassert>

float g_Time = 0.f;
float animF = 0.f;
bool Up = TRUE;

Renderer::Renderer(int windowSizeX, int windowSizeY)
{
	Initialize(windowSizeX, windowSizeY);
}


Renderer::~Renderer()
{
}

void Renderer::Initialize(int windowSizeX, int windowSizeY)
{
	//Set window size
	m_WindowSizeX = windowSizeX;
	m_WindowSizeY = windowSizeY;

	//Load shaders
	m_SolidRectShader = CompileShaders("./Shaders/SolidRect.vs", "./Shaders/SolidRect.fs");
	m_SimpleVelShader = CompileShaders("./Shaders/SimpleVel.vs", "./Shaders/SimpleVel.fs");
	m_SinTrailShader = CompileShaders("./Shaders/SinTrail.vs", "./Shaders/SinTrail.fs");
	m_Lecture8Shader = CompileShaders("./Shaders/Lecture8.vs", "./Shaders/Lecture8.fs");
	m_FillAllShader = CompileShaders("./Shaders/FillAll.vs", "./Shaders/FillAll.fs");
	m_SinShader = CompileShaders("./Shaders/Sin.vs", "./Shaders/Sin.fs");
	m_TextureSampleShader = CompileShaders("./Shaders/TextureSample.vs", "./Shaders/TextureSample.fs");
	m_DrawNumberShader = CompileShaders("./Shaders/DrawNumber.vs", "./Shaders/DrawNumber.fs");
	m_SpriteAnimShader = CompileShaders("./Shaders/SpriteAnim.vs", "./Shaders/SpriteAnim.fs");
	m_SandboxShader = CompileShaders("./Shaders/Sandbox.vs", "./Shaders/Sandbox.fs");
	m_Proj_Shader = CompileShaders("./Shaders/Cube.vs", "./Shaders/Cube.fs");
	m_HeightmapShader = CompileShaders("./Shaders/Heightmap.vs", "./Shaders/Heightmap.fs");
	m_DrawTextureRectShader = CompileShaders("./Shaders/DrawTextureRect.vs", "./Shaders/DrawTextureRect.fs");
	m_HDRTextureRectShader = CompileShaders("./Shaders/HDRTextureRect.vs", "./Shaders/HDRTextureRect.fs");
	m_ParticleAnimShader = CompileShaders("./Shaders/ParticleAnim_01.vs", "./Shaders/ParticleAnim_01.fs");

	// Load Textures
	m_CloudTexture = CreatePngTexture("./Textures/T_Cloud.png");
	m_PororoTexture = CreatePngTexture("./Textures/T_Pororo.png");
	m_RGBTexture = CreatePngTexture("./Textures/T_RGB.png");

	m_WoodTexture[0] = CreatePngTexture("./Textures/T_Wood_0.png");
	m_WoodTexture[1] = CreatePngTexture("./Textures/T_Wood_1.png");
	m_WoodTexture[2] = CreatePngTexture("./Textures/T_Wood_2.png");
	m_WoodTexture[3] = CreatePngTexture("./Textures/T_Wood_3.png");
	m_WoodTexture[4] = CreatePngTexture("./Textures/T_Wood_4.png");
	m_WoodTexture[5] = CreatePngTexture("./Textures/T_Wood_5.png");
	m_WoodTexture[6] = CreatePngTexture("./Textures/T_Wood_6.png");
	m_WoodTexture[7] = CreatePngTexture("./Textures/T_Wood_7.png");
	m_WoodTexture[8] = CreatePngTexture("./Textures/T_Wood_8.png");

	m_ZeroToTen = CreatePngTexture("./Textures/T_ZeroToTen.png");

	m_SpriteAnimTexture = CreatePngTexture("./Textures/T_SpriteAnim.png");
	m_FlagTexture = CreatePngTexture("./Textures/T_Flag.png");

	m_HeightmapTexture = CreatePngTexture("./Textures/T_Heightmap.png");
	m_SnowTexture = CreatePngTexture("./Textures/T_Snow.png");
	m_GrassTexture = CreatePngTexture("./Textures/T_Grass.png");

	m_Particle1Texture = CreatePngTexture("./Textures/T_Cloud.png");

	/*
	m_WoodTexture_0 = CreatePngTexture("./Textures/T_Wood_0.png");
	m_WoodTexture_1 = CreatePngTexture("./Textures/T_Wood_1.png");
	m_WoodTexture_2 = CreatePngTexture("./Textures/T_Wood_2.png");
	m_WoodTexture_3 = CreatePngTexture("./Textures/T_Wood_3.png");
	m_WoodTexture_4 = CreatePngTexture("./Textures/T_Wood_4.png");
	m_WoodTexture_5 = CreatePngTexture("./Textures/T_Wood_5.png");
	m_WoodTexture_6 = CreatePngTexture("./Textures/T_Wood_6.png");
	m_WoodTexture_7 = CreatePngTexture("./Textures/T_Wood_7.png");
	m_WoodTexture_8 = CreatePngTexture("./Textures/T_Wood_8.png");
	*/


	//Create VBOs
	CreateVertexBufferObjects();
	//Lecture7VBOs(RectNum, FALSE, TRUE);
	//Lecture8VBOs();
	//Lecture9_Sin_VBOs();
	//Lecture10_CheckerBoard_VBOs();
	//Lecture8_MultipleTextures_VBOs();
	Lecture8VBOs();
	//Lecture7VBOs(5000 , TRUE, TRUE);
	Lecture10_CheckerBoard_VBOs(); // 텍스쳐 좌표 사용
	m_FBO0 = CreateFBO(512, 512, &m_FBOTexture0, TRUE);
	m_FBO1 = CreateFBO(512, 512, &m_FBOTexture1, TRUE);
	m_FBO2 = CreateFBO(512, 512, &m_FBOTexture2, FALSE);
	m_FBO3 = CreateFBO(512, 512, &m_FBOTexture3, FALSE);

}

void Renderer::CreateVertexBufferObjects()
{
	float size = 1.f;
	float rect[]
		=
	{
		-size, -size, 0.f,
		size, -size, 0.f,
		size, size, 0.f,//Triangle1 (X, Y, Z, Value)

		-size, -size, 0.f,
		-size, size, 0.f,
		size, size, 0.f//Triangle2
	};

	glGenBuffers(1, &m_VBORect);	// Generate
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.

	float color[]
		=
	{
		1, 0, 0, 1,	// R, G, B, A
		1, 0, 0, 1,
		1, 0, 0, 1,
		1, 0, 0, 1,
		1, 0, 0, 1,
		1, 0, 0, 1,
	};

	glGenBuffers(1, &m_VBORectColor);	// Generate
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORectColor);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(color), color, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.

	//lecture2
	float triangleVertex[]
		=
	{
		-1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, // 9 floats
	};

	glGenBuffers(1, &m_VBOLecture);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOLecture);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangleVertex), triangleVertex, GL_STATIC_DRAW);

	GenQuadsVBO(100, true, &m_VBOQuads1, &m_VBOQuads_VertexCount1);

	InitMatricies();
	CreateGridMesh();


}

void Renderer::GenQuadsVBO(int count, bool bRandPos, GLuint * id, GLuint * vCount)
{
	float size = 0.001f;
	int countQuad = count;
	int verticesPerQuad = 6;
	int floatsPerVertex = 3 + 3 + 2 + 2 + 1 + 4; //x,y,z, vx,vy,vz, s,l, r,a, randValue, r, g, b, a
	int arraySize = countQuad * verticesPerQuad * floatsPerVertex;
	float *vertices = new float[arraySize];

	for (int i = 0; i < countQuad; i++)
	{
		float randX, randY;
		float randVelX, randVelY, randVelZ;
		float startTime, lifeTime;
		float startTimeMax = 6.f;
		float lifeTimeMax = 3.f;
		float ratio, amp;
		float ratioMin = 2.f;
		float ratioThres = 4.f;
		float ampMin = -0.1f;
		float ampThres = 0.2f;
		float randValue = 0.f, randThres = 1.f;
		float r, g, b, a;

		int index = i * verticesPerQuad * floatsPerVertex;

		if (bRandPos)
		{
			randX = 2.f*(((float)rand() / (float)RAND_MAX) - 0.5f);
			randY = 2.f*(((float)rand() / (float)RAND_MAX) - 0.5f);
		}
		else
		{
			randX = 0.f;
			randY = 0.f;
		}

		randVelX = 0.1f*(((float)rand() / (float)RAND_MAX) - 0.5f);
		randVelY = 0.1f*(((float)rand() / (float)RAND_MAX) - 0.5f);
		randVelZ = 0.f;

		startTime = ((float)rand() / (float)RAND_MAX) * startTimeMax;
		lifeTime = ((float)rand() / (float)RAND_MAX) * lifeTimeMax;

		ratio = ratioMin + ((float)rand() / (float)RAND_MAX) * ratioThres;
		amp = ampMin + ((float)rand() / (float)RAND_MAX) * ampThres;

		randValue = randValue + ((float)rand() / (float)RAND_MAX) * randThres;

		r = ((float)rand() / (float)RAND_MAX) + 2.0;
		g = ((float)rand() / (float)RAND_MAX);
		b = ((float)rand() / (float)RAND_MAX);
		a = 1.0;

		/*
		r = ((float)rand() / (float)RAND_MAX);
		g = ((float)rand() / (float)RAND_MAX);
		b = ((float)rand() / (float)RAND_MAX);
		a = 1.f;
		*/

		vertices[index] = randX - size; index++;
		vertices[index] = randY - size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;

		vertices[index] = randX - size; index++;
		vertices[index] = randY + size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;

		vertices[index] = randX + size; index++;
		vertices[index] = randY + size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;

		vertices[index] = randX - size; index++;
		vertices[index] = randY - size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;

		vertices[index] = randX + size; index++;
		vertices[index] = randY + size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;

		vertices[index] = randX + size; index++;
		vertices[index] = randY - size; index++;
		vertices[index] = 0.f; index++;
		vertices[index] = randVelX; index++;
		vertices[index] = randVelY; index++;
		vertices[index] = randVelZ; index++;
		vertices[index] = startTime; index++;
		vertices[index] = lifeTime; index++;
		vertices[index] = ratio; index++;
		vertices[index] = amp; index++;
		vertices[index] = randValue; index++;
		vertices[index] = r; index++;
		vertices[index] = g; index++;
		vertices[index] = b; index++;
		vertices[index] = a; index++;
	}

	GLuint vboID = 0;

	glGenBuffers(1, &vboID);
	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*arraySize, vertices, GL_STATIC_DRAW);
	*vCount = countQuad * verticesPerQuad;
	*id = vboID;
}

void Renderer::AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType)
{
	//쉐이더 오브젝트 생성
	GLuint ShaderObj = glCreateShader(ShaderType);

	if (ShaderObj == 0) {
		fprintf(stderr, "Error creating shader type %d\n", ShaderType);
	}

	const GLchar* p[1];
	p[0] = pShaderText;
	GLint Lengths[1];
	Lengths[0] = (GLint)strlen(pShaderText);
	//쉐이더 코드를 쉐이더 오브젝트에 할당
	glShaderSource(ShaderObj, 1, p, Lengths);

	//할당된 쉐이더 코드를 컴파일
	glCompileShader(ShaderObj);

	GLint success;
	// ShaderObj 가 성공적으로 컴파일 되었는지 확인
	glGetShaderiv(ShaderObj, GL_COMPILE_STATUS, &success);
	if (!success) {
		GLchar InfoLog[1024];

		//OpenGL 의 shader log 데이터를 가져옴
		glGetShaderInfoLog(ShaderObj, 1024, NULL, InfoLog);
		fprintf(stderr, "Error compiling shader type %d: '%s'\n", ShaderType, InfoLog);
		printf("%s \n", pShaderText);
	}

	// ShaderProgram 에 attach!!
	glAttachShader(ShaderProgram, ShaderObj);
}

bool Renderer::ReadFile(char* filename, std::string *target)
{
	std::ifstream file(filename);
	if (file.fail())
	{
		std::cout << filename << " file loading failed.. \n";
		file.close();
		return false;
	}
	std::string line;
	while (getline(file, line)) {
		target->append(line.c_str());
		target->append("\n");
	}
	return true;
}

GLuint Renderer::CompileShaders(char* filenameVS, char* filenameFS)
{
	GLuint ShaderProgram = glCreateProgram(); //빈 쉐이더 프로그램 생성

	if (ShaderProgram == 0) { //쉐이더 프로그램이 만들어졌는지 확인
		fprintf(stderr, "Error creating shader program\n");
	}

	std::string vs, fs;	// 텍스트 형태로 저장된 vs, fs

	//shader.vs 가 vs 안으로 로딩됨
	if (!ReadFile(filenameVS, &vs)) {
		printf("Error compiling vertex shader\n");
		return -1;
	};

	//shader.fs 가 fs 안으로 로딩됨
	if (!ReadFile(filenameFS, &fs)) {
		printf("Error compiling fragment shader\n");
		return -1;
	};

	// ShaderProgram 에 vs.c_str() 버텍스 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, vs.c_str(), GL_VERTEX_SHADER);

	// ShaderProgram 에 fs.c_str() 프레그먼트 쉐이더를 컴파일한 결과를 attach함
	AddShader(ShaderProgram, fs.c_str(), GL_FRAGMENT_SHADER);

	GLint Success = 0;
	GLchar ErrorLog[1024] = { 0 };

	//Attach 완료된 shaderProgram 을 링킹함
	glLinkProgram(ShaderProgram);

	//링크가 성공했는지 확인
	glGetProgramiv(ShaderProgram, GL_LINK_STATUS, &Success);

	if (Success == 0) {
		// shader program 로그를 받아옴
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error linking shader program\n" << ErrorLog;
		return -1;
	}

	glValidateProgram(ShaderProgram);
	glGetProgramiv(ShaderProgram, GL_VALIDATE_STATUS, &Success);
	if (!Success) {
		glGetProgramInfoLog(ShaderProgram, sizeof(ErrorLog), NULL, ErrorLog);
		std::cout << filenameVS << ", " << filenameFS << " Error validating shader program\n" << ErrorLog;
		return -1;
	}

	glUseProgram(ShaderProgram);
	std::cout << filenameVS << ", " << filenameFS << " Shader compiling is done.\n";

	return ShaderProgram;
}
unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight)
{
	std::cout << "Loading bmp file " << imagepath << " ... " << std::endl;
	outWidth = -1;
	outHeight = -1;
	// Data read from the header of the BMP file
	unsigned char header[54];
	unsigned int dataPos;
	unsigned int imageSize;
	// Actual RGB data
	unsigned char * data;

	// Open the file
	FILE * file = NULL;
	fopen_s(&file, imagepath, "rb");
	if (!file)
	{
		std::cout << "Image could not be opened, " << imagepath << " is missing. " << std::endl;
		return NULL;
	}

	if (fread(header, 1, 54, file) != 54)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (header[0] != 'B' || header[1] != 'M')
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1E]) != 0)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	if (*(int*)&(header[0x1C]) != 24)
	{
		std::cout << imagepath << " is not a correct BMP file. " << std::endl;
		return NULL;
	}

	dataPos = *(int*)&(header[0x0A]);
	imageSize = *(int*)&(header[0x22]);
	outWidth = *(int*)&(header[0x12]);
	outHeight = *(int*)&(header[0x16]);

	if (imageSize == 0)
		imageSize = outWidth * outHeight * 3;

	if (dataPos == 0)
		dataPos = 54;

	data = new unsigned char[imageSize];

	fread(data, 1, imageSize, file);

	fclose(file);

	std::cout << imagepath << " is succesfully loaded. " << std::endl;

	return data;
}

GLuint Renderer::CreatePngTexture(char * filePath)
{
	//Load Pngs: Load file and decode image.
	std::vector<unsigned char> image;
	unsigned width, height;
	unsigned error = lodepng::decode(image, width, height, filePath);
	if (error != 0)
	{
		lodepng_error_text(error);
		assert(error == 0);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &image[0]);

	return temp;
}

GLuint Renderer::CreateBmpTexture(char * filePath)
{
	//Load Bmp: Load file and decode image.
	unsigned int width, height;
	unsigned char * bmp
		= loadBMPRaw(filePath, width, height);

	if (bmp == NULL)
	{
		std::cout << "Error while loading bmp file : " << filePath << std::endl;
		assert(bmp != NULL);
		return -1;
	}

	GLuint temp;
	glGenTextures(1, &temp);

	glBindTexture(GL_TEXTURE_2D, temp);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, bmp);

	return temp;
}

void Renderer::GenRectsVBOs(int count)
{
	int countQuad = count;
	int verticesPerQuad = 6;
	int floatsPerVertex = 3 + 3 + 2;	// 위치(3), 속도(3), 시간(2)
	int arraysize = countQuad * verticesPerQuad * floatsPerVertex;


	float randX, randY;
	float Z = 0.f;
	float size = 0.01f;

	float ranVelx, ranVely, ranVelz;
	
	// 시작 시간, 끝나는 시간
	float startTime, lifeTime;
	float startTimeMax = 3.f;
	float lifeTimeMax = 3.f;

	float *verticies = new float[arraysize];

	int index = 0;


	// 포문 한번 돌 때 마다 사각형 생성됨
	for (int i = 0; i < count; i++) {
		randX = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
		randY = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;

		ranVelx = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
		ranVely = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
		ranVelz = 0.f;

		startTime = (float(rand()) / float(RAND_MAX)) * startTimeMax;
		lifeTime = (float(rand()) / float(RAND_MAX) + 0.1f) * lifeTimeMax;

		index = i * floatsPerVertex * verticesPerQuad;	// i * 사각형당 필요한 float 개수 * 버텍스 개수

		//std::cout << i << "번째 " <<  "randY = " << randY << std::endl;

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;


		verticies[index] = randX + size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;


		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		// ---

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;



		verticies[index] = randX - size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;



		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

	}

	// VBO 생성
	glGenBuffers(1, &m_VBOGenRandRects);	// Vertex Buffer 객체를 생성한다.
	
	// 생성된 VBO를 불러와서 어떤 목적으로 사용할 것인지 정의
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);	// 생성된 Vertex Buffer에 어떤 목적으로 쓸것인지 알려준다. (여기서는 정점 데이터를 생성한 VBOGenRandRects에 넣는다는 뜻)
	
	// VBO에 어떻게 데이터를 넣을 것인지 설정
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);	// 두번째 size 인자는, 그릴 모든 vertex의 크기의 총 합을 뜻한다. 

	m_VBOGenRandRects_VertexCount = countQuad * verticesPerQuad;
}

void Renderer::Lecture6VBOs(int count, bool bRandPos)
{
	int countQuad = count;
	int verticesPerQuad = 6;
	int floatsPerVertex = 3 + 3 + 2 + 2 + 1;	// 위치(3), 속도(3), 시간(2), 주기+진폭(2), Value(1)
	int arraysize = countQuad * verticesPerQuad * floatsPerVertex;


	float randX, randY;
	float Z = 0.f;
	float size = 0.001f;

	float ranVelx, ranVely, ranVelz;

	// 시작 시간, 끝나는 시간
	float startTime, lifeTime;
	float startTimeMax = 3.f;
	float lifeTimeMax = 3.f;

	float ratio, amplitude;
	float ratioMin = 2.f;
	float ratioThres = 4.f;
	float amplitudeMin = 0.f;
	float amplitudeThres = 0.2f;

	float value = 0.f;

	float *verticies = new float[arraysize];

	int index = 0;


	// 포문 한번 돌 때 마다 사각형 생성됨
	for (int i = 0; i < count; i++) {

		if (bRandPos)
		{
			randX = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
			randY = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
		}

		else
		{
			randX = 0.f;
			randY = 0.f;
		}

		ranVelx = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.1f;
		ranVely = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.1f;
		ranVelz = 0;

		startTime = (float(rand()) / float(RAND_MAX)) * startTimeMax;
		lifeTime = (float(rand()) / float(RAND_MAX)) * lifeTimeMax;

		ratio = (float(rand()) / float(RAND_MAX)) * ratioThres;
		amplitude = (float(rand()) / float(RAND_MAX)) * amplitudeThres;

		value = (float(rand()) / float(RAND_MAX)) * 1.f;

		index = i * floatsPerVertex * verticesPerQuad;	// i * 사각형당 필요한 float 개수 * 버텍스 개수

		//std::cout << i << "번째 " <<  "randY = " << randY << std::endl;

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;



		verticies[index] = randX + size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;



		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;
		
		verticies[index] = value; index++;


		// ---

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;




		verticies[index] = randX - size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;




		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;


	}

	// VBO 생성
	glGenBuffers(1, &m_VBOGenRandRects);	// Vertex Buffer 객체를 생성한다.

	// 생성된 VBO를 불러와서 어떤 목적으로 사용할 것인지 정의
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);	// 생성된 Vertex Buffer에 어떤 목적으로 쓸것인지 알려준다. (여기서는 정점 데이터를 생성한 VBOGenRandRects에 넣는다는 뜻)

	// VBO에 어떻게 데이터를 넣을 것인지 설정
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);	// 두번째 size 인자는, 그릴 모든 vertex의 크기의 총 합을 뜻한다. 

	m_VBOGenRandRects_VertexCount = countQuad * verticesPerQuad;

	//*vCount = countQuad * verticiesPerQuad;
	//*id = vboID;
}

void Renderer::Lecture7VBOs(int count, bool bRandPos, bool bRandColor)
{
	int countQuad = count;

	int floatsPerVertex = 3 + 3 + 4 + 4 + 1;
	int verticiesPerQuad = 6;
	int arraySize = verticiesPerQuad * floatsPerVertex * countQuad;

	float* verticies = new float[arraySize];

	int index = 0;

	float randX, randY, Z;
	float size = 0.05f;

	float velX, velY, velZ;
	float velMax = 0.5f;

	float startTime, lifeTime;
	float startTimeMax = 3.f;
	float lifeTimeMin = 1.f, lifeTimeMax = 5.f;

	float ratio, amplitude;
	float ratioMin = 0.5f, ratioThres = 2.f;				// 주기
	float amplitudeMin = 0.1f, amplitudeThres = 0.5;	// 진폭

	float randValue = 0.1f;
	float randThres = 1.f;


	float R, G, B, A;

	for (int i = 0; i < countQuad; i++)
	{
		if (bRandPos)
		{
			randX = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
			randY = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
			Z = 0.f;
		}
		else
		{
			randX = 0.f;
			randY = 0.f;
			Z = 0.f;
		}

		velX = (float(rand()) / float(RAND_MAX) - 0.5f) * velMax;
		velY = (float(rand()) / float(RAND_MAX) - 0.5f) * velMax;
		velZ = 0.f;

		startTime = (float(rand() / float(RAND_MAX))) * startTimeMax;
		lifeTime = (float(rand() / float(RAND_MAX))) * lifeTimeMax + lifeTimeMin;
		ratio = (float(rand() / float(RAND_MAX))) * ratioThres + ratioMin;
		amplitude = (float(rand() / float(RAND_MAX))) * amplitudeThres + amplitudeMin;


		if (bRandColor)
		{
			R = (float(rand()) / float(RAND_MAX));
			G = (float(rand()) / float(RAND_MAX));
			B = (float(rand()) / float(RAND_MAX));
			A = (float(rand()) / float(RAND_MAX) + 0.5f) * 2.f;
		}
		else
		{
			R = 0.f;
			G = 1.f;
			B = 0.f;
			A = 1.f;
		}

		randValue = (float(rand()) / float(RAND_MAX)) * randThres;



		index = i * verticiesPerQuad * floatsPerVertex;

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;


		verticies[index] = randX - size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;




		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;





		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;




		verticies[index] = randX + size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;




		verticies[index] = randX + size; index++;
		verticies[index] = randY + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = velX; index++;
		verticies[index] = velY; index++;
		verticies[index] = velZ; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;
		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;

		verticies[index] = randValue; index++;


	}

	glGenBuffers(1, &m_VBOSinTrail);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSinTrail);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);


	m_sumOfVerticiesForQuads = countQuad * verticiesPerQuad;
}
/*
void Renderer::Lecture7VBOs(int count, bool bRandPos)
{
	int countQuad = count;
	int verticesPerQuad = 6;
	int floatsPerVertex = 3 + 3 + 2 + 2 + 1 + 4;	// 위치(3), 속도(3), 시간(2), 주기+진폭(2), Value(1), Color(4)
	int arraysize = countQuad * verticesPerQuad * floatsPerVertex;


	float randX, randY;
	float Z = 0.f;
	float size = 0.001f;

	float ranVelx, ranVely, ranVelz;

	// 시작 시간, 끝나는 시간
	float startTime, lifeTime;
	float startTimeMax = 3.f;
	float lifeTimeMax = 3.f;

	float ratio, amplitude;
	float ratioMin = 2.f;
	float ratioThres = 4.f;
	float amplitudeMin = 0.f;
	float amplitudeThres = 0.2f;

	float value = 0.f;

	float R, G, B, A = 0.f;



	float *verticies = new float[arraysize];

	int index = 0;


	// 포문 한번 돌 때 마다 사각형 생성됨
	for (int i = 0; i < count; i++) {

		if (bRandPos)
		{
			randX = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
			randY = (float(rand()) / float(RAND_MAX) - 0.5f) * 2.f;
		}

		else
		{
			randX = 0.f;
			randY = 0.f;
		}

		ranVelx = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.1f;
		ranVely = (float(rand()) / float(RAND_MAX) - 0.5f) * 0.1f;
		ranVelz = 0;

		startTime = (float(rand()) / float(RAND_MAX)) * startTimeMax;
		lifeTime = (float(rand()) / float(RAND_MAX)) * lifeTimeMax;

		ratio = (float(rand()) / float(RAND_MAX)) * ratioThres;
		amplitude = (float(rand()) / float(RAND_MAX)) * amplitudeThres;

		value = (float(rand()) / float(RAND_MAX)) * 1.f;

		//R = (float(rand()) / float(RAND_MAX)) * 1.f;
		//G = (float(rand()) / float(RAND_MAX)) * 1.f;
		//B = (float(rand()) / float(RAND_MAX)) * 1.f;
		//A = (float(rand()) / float(RAND_MAX)) * 1.f;
		R = 1.f;
		G = 1.f;
		B = 1.f;
		A = 1.f;




		index = i * floatsPerVertex * verticesPerQuad;	// i * 사각형당 필요한 float 개수 * 버텍스 개수

		//std::cout << i << "번째 " <<  "randY = " << randY << std::endl;

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;




		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;



		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;


		// ---

		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;




		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;




		verticies[index] = randX - size; index++;
		verticies[index] = randY - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = ranVelx; index++;
		verticies[index] = ranVely; index++;
		verticies[index] = ranVelz; index++;

		verticies[index] = startTime; index++;
		verticies[index] = lifeTime; index++;

		verticies[index] = ratio; index++;
		verticies[index] = amplitude; index++;

		verticies[index] = value; index++;

		verticies[index] = R; index++;
		verticies[index] = G; index++;
		verticies[index] = B; index++;
		verticies[index] = A; index++;


	}

	// VBO 생성
	glGenBuffers(1, &m_VBOGenRandRects);	// Vertex Buffer 객체를 생성한다.

	// 생성된 VBO를 불러와서 어떤 목적으로 사용할 것인지 정의
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);	// 생성된 Vertex Buffer에 어떤 목적으로 쓸것인지 알려준다. (여기서는 정점 데이터를 생성한 VBOGenRandRects에 넣는다는 뜻)

	// VBO에 어떻게 데이터를 넣을 것인지 설정
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);	// 두번째 size 인자는, 그릴 모든 vertex의 크기의 총 합을 뜻한다. 

	m_VBOGenRandRects_VertexCount = countQuad * verticesPerQuad;

	//*vCount = countQuad * verticiesPerQuad;
	//*id = vboID;
}
*/



void Renderer::Test()
{
	if (Up == TRUE)
		animF += 0.01f;

	if (animF > 1.0f)
		Up = FALSE;
		
	
	if (Up == FALSE )
		animF -= 0.01f;

	if (animF < 0.0f)
		Up = TRUE;

	glUseProgram(m_SolidRectShader);

	GLuint uTime = glGetUniformLocation(m_SolidRectShader, "u_Time");
	glUniform1f(uTime, animF);		// 이걸 이용해서 버텍스 셰이더에 시간값을 넣을 수 있음.

	GLuint aPos = glGetAttribLocation(m_SolidRectShader, "a_Position");	// m_SolidRectShader 프로그램 객체로부터 이름이 a_Position인 속성 변수가 바인딩 된 인덱스를 리턴받아서 attribPosition라는 인덱스 버퍼에 저장한다.
	GLuint aColor = glGetAttribLocation(m_SolidRectShader, "a_Color");

	
	glEnableVertexAttribArray(aPos);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);	
	glVertexAttribPointer(aPos, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);	// 실제 데이터는 sizof(float) * 4 이 부분을 고쳐야 몇개씩 읽을지 정하는 것임.
	//glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(aColor);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBORectColor);
	glVertexAttribPointer(aColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, 0);


	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aColor);
}

void Renderer::Lecture2()
{
	
	glUseProgram(0);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);
	
	glDrawArrays(GL_TRIANGLES, 0, m_VBOGenRandRects_VertexCount);

	glDisableVertexAttribArray(0);
}

void Renderer::CreateGridMesh()
{
	float basePosX = -0.5f;
	float basePosY = -0.5f;
	float targetPosX = 0.5f;
	float targetPosY = 0.5f;

	int pointCountX = 200;
	int pointCountY = 200;

	float width = targetPosX - basePosX;
	float height = targetPosY - basePosY;

	float* point = new float[pointCountX*pointCountY * 2];
	float* vertices = new float[(pointCountX - 1)*(pointCountY - 1) * 2 * 3 * 3];
	m_VBOGridMesh_Count = (pointCountX - 1)*(pointCountY - 1) * 2 * 3;

	//Prepare points
	for (int x = 0; x < pointCountX; x++)
	{
		for (int y = 0; y < pointCountY; y++)
		{
			point[(y*pointCountX + x) * 2 + 0] = basePosX + width * (x / (float)(pointCountX - 1));
			point[(y*pointCountX + x) * 2 + 1] = basePosY + height * (y / (float)(pointCountY - 1));
		}
	}

	//Make triangles
	int vertIndex = 0;
	for (int x = 0; x < pointCountX - 1; x++)
	{
		for (int y = 0; y < pointCountY - 1; y++)
		{
			//Triangle part 1
			vertices[vertIndex] = point[(y*pointCountX + x) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[(y*pointCountX + x) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + (x + 1)) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + (x + 1)) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + x) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + x) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;

			//Triangle part 2
			vertices[vertIndex] = point[(y*pointCountX + x) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[(y*pointCountX + x) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;
			vertices[vertIndex] = point[(y*pointCountX + (x + 1)) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[(y*pointCountX + (x + 1)) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + (x + 1)) * 2 + 0];
			vertIndex++;
			vertices[vertIndex] = point[((y + 1)*pointCountX + (x + 1)) * 2 + 1];
			vertIndex++;
			vertices[vertIndex] = 0.f;
			vertIndex++;
		}
	}

	glGenBuffers(1, &m_VBOGridMesh);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGridMesh);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float)*(pointCountX - 1)*(pointCountY - 1) * 2 * 3 * 3, vertices, GL_STATIC_DRAW);
}

void Renderer::Lecture3()
{
	glUseProgram(0);

	CreateGridMesh();
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGridMesh);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_LINE_STRIP, 0, m_VBOGridMesh_Count);

	glDisableVertexAttribArray(0);
}

void Renderer::Lecture4()
{

	std::cout << g_Time << std::endl;
	glUseProgram(m_SimpleVelShader);

	GLuint uTime = glGetUniformLocation(m_SimpleVelShader, "u_Time");
	glUniform1f(uTime, g_Time);

	g_Time += 0.01f;
	if (g_Time > 3.0f)
		g_Time = 0;

	GLuint aPos = glGetAttribLocation(m_SimpleVelShader, "a_Position");
	GLuint aVel = glGetAttribLocation(m_SimpleVelShader, "a_Vel");


	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aVel);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);
	// x, y, z, x1, y1, z1 에서
	// x, y, z, vx, vy, vz, x1, y1, z1 으로 변경됨.
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, 0);	// STRIDE, 해당하는 데이터를 읽으려면 몇번 뛰어넘어야하나?

	// 최초 시작 지점을 vx로 바꿔야 함 (밀어줘야함)
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 6, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, m_VBOGenRandRects_VertexCount);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aVel);

}

void Renderer::Lecture5()
{

	glUseProgram(m_SimpleVelShader);
	g_Time += 0.01f;
	GLuint uTime = glGetUniformLocation(m_SimpleVelShader, "u_elapsedTime");
	GLuint uRepeat = glGetUniformLocation(m_SimpleVelShader, "u_Repeat");
	glUniform1f(uTime, g_Time);


	GLuint aPos = glGetAttribLocation(m_SimpleVelShader, "a_Position");
	GLuint aVel = glGetAttribLocation(m_SimpleVelShader, "a_Vel");
	GLuint StartLife = glGetAttribLocation(m_SimpleVelShader, "a_StartLife");
	//GLuint aEmit_T = glGetAttribLocation(m_SimpleVelShader, "a_EmitTime");
	//GLuint aLife_T = glGetAttribLocation(m_SimpleVelShader, "a_LifeTime");


	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aVel);
	glEnableVertexAttribArray(StartLife);


	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);
	// x, y, z, x1, y1, z1 에서
	// x, y, z, vx, vy, vz, x1, y1, z1 으로 변경됨.
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, 0);	// STRIDE, 해당하는 데이터를 읽으려면 몇번 뛰어넘어야하나?

	// 최초 시작 지점을 vx로 바꿔야 함 (밀어줘야함)
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (GLvoid*)(sizeof(float) * 3));

	glVertexAttribPointer(StartLife, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 8, (GLvoid*)(sizeof(float) * 6));


	glDrawArrays(GL_TRIANGLES, 0, m_VBOGenRandRects_VertexCount);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aVel);
	glDisableVertexAttribArray(StartLife);


}



void Renderer::Lecture6()
{
	GLuint shader = m_SinTrailShader;

	glUseProgram(shader);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;

	//std::cout << g_Time << std::endl;
	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aVel = glGetAttribLocation(shader, "a_Vel");
	GLuint aStartLifeRatioAmp = glGetAttribLocation(shader, "a_StartLifeRatioAmp");
	GLuint aRandValue = glGetAttribLocation(shader, "a_RandValue");


	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aVel);
	glEnableVertexAttribArray(aStartLifeRatioAmp);
	glEnableVertexAttribArray(aRandValue);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 11, 0);
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(aStartLifeRatioAmp, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (GLvoid*)(sizeof(float) * 6));
	glVertexAttribPointer(aRandValue, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 11, (GLvoid*)(sizeof(float) * 10));



	glDrawArrays(GL_TRIANGLES, 0, m_VBOGenRandRects_VertexCount);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aVel);
	glDisableVertexAttribArray(aStartLifeRatioAmp);
	glDisableVertexAttribArray(aRandValue);
}


void Renderer::Lecture7()
{
	GLuint Shader = m_ParticleAnimShader;
	// 셰이더 프로그램 지정해줘야 함
	glUseProgram(Shader);

	//glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(Shader, "u_ElapsedTime");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;

	GLuint uTex = glGetUniformLocation(Shader, "u_Texture");
	glUniform1i(uTex, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Particle1Texture);

	GLuint aPosition = glGetAttribLocation(Shader, "a_Position");
	GLuint aVel = glGetAttribLocation(Shader, "a_Vel");
	GLuint aColor = glGetAttribLocation(Shader, "a_Color");
	GLuint aStartLifeRatioAmp = glGetAttribLocation(Shader, "a_StartLifeRatioAmp");
	GLuint aRandValue = glGetAttribLocation(Shader, "a_RandValue");

	glEnableVertexAttribArray(aPosition);
	glEnableVertexAttribArray(aVel);
	glEnableVertexAttribArray(aColor);
	glEnableVertexAttribArray(aStartLifeRatioAmp);
	glEnableVertexAttribArray(aRandValue);


	glBindBuffer(GL_ARRAY_BUFFER, m_VBOSinTrail);
	glVertexAttribPointer(aPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, 0);
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(aStartLifeRatioAmp, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 6));
	glVertexAttribPointer(aColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 10));
	glVertexAttribPointer(aRandValue, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15, (GLvoid*)(sizeof(float) * 14));



	// 모든 쿼드에 필요한 버텍스의 개수 넣어줘야 함.
	glDrawArrays(GL_TRIANGLES, 0, m_sumOfVerticiesForQuads);
	glDisableVertexAttribArray(aPosition);
	glDisableVertexAttribArray(aVel);
	glDisableVertexAttribArray(aColor);
	glDisableVertexAttribArray(aStartLifeRatioAmp);
	glDisableVertexAttribArray(aRandValue);


}

void Renderer::Lecture8VBOs()
{
	int countQuads = 1;
	int VerticiesPerQuad = 6;
	int floatsPerVertex = 3 + 2;

	int index = 0;

	int arraySize = countQuads * VerticiesPerQuad * floatsPerVertex;

	float *verticies = new float[arraySize];

	float X = 0.f, Y = 0.f, Z = 0.0f;
	float U = 0.f;
	float V = 0.f;
	float size = 1.f;
	float uvSize = 1.f;

	for (int i = 0; i < countQuads; i++)
	{
		index = i * floatsPerVertex * VerticiesPerQuad;

		
		verticies[index] = X - size; index++;
		verticies[index] = Y - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = 0; index++;
		verticies[index] = 0; index++;




		verticies[index] = X + size; index++;
		verticies[index] = Y - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = 1; index++;
		verticies[index] = 0; index++;




		verticies[index] = X + size; index++;
		verticies[index] = Y + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = 1; index++;
		verticies[index] = 1; index++;




		verticies[index] = X - size; index++;
		verticies[index] = Y - size; index++;
		verticies[index] = Z; index++;

		verticies[index] = 0; index++;
		verticies[index] = 0; index++;




		verticies[index] = X - size; index++;
		verticies[index] = Y + size; index++;
		verticies[index] = Z; index++;
		
		verticies[index] = 0; index++;
		verticies[index] = 1; index++;




		verticies[index] = X + size; index++;
		verticies[index] = Y + size; index++;
		verticies[index] = Z; index++;

		verticies[index] = 1; index++;
		verticies[index] = 1; index++;



		

		/*
		verticies[index] = -0.5f; index++;
		verticies[index] = -0.5f; index++;
		verticies[index] = Z; index++;

		verticies[index] = 0.5f; index++;
		verticies[index] = -0.5f; index++;
		verticies[index] = Z; index++;

		verticies[index] = 0.5f; index++;
		verticies[index] = 0.5f; index++;
		verticies[index] = Z; index++;

		verticies[index] = -0.5f; index++;
		verticies[index] = -0.5f; index++;
		verticies[index] = Z; index++;

		verticies[index] = -0.5f; index++;
		verticies[index] = 0.5f; index++;
		verticies[index] = Z; index++;

		verticies[index] = 0.5f; index++;
		verticies[index] = 0.5f; index++;
		verticies[index] = Z; index++;
		*/
	}

	glGenBuffers(1, &m_VBOGenRandRects);	// Vertex Buffer 객체를 생성한다.

	// 생성된 VBO를 불러와서 어떤 목적으로 사용할 것인지 정의
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);	// 생성된 Vertex Buffer에 어떤 목적으로 쓸것인지 알려준다. (여기서는 정점 데이터를 생성한 VBOGenRandRects에 넣는다는 뜻)

	// VBO에 어떻게 데이터를 넣을 것인지 설정
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * index, verticies, GL_STATIC_DRAW);	// 두번째 size 인자는, 그릴 모든 vertex의 크기의 총 합을 뜻한다. 

}


void Renderer::FillAll(float alpha)
{
	GLuint Shader = m_FillAllShader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	glEnableVertexAttribArray(aPos);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBORect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
}

void Renderer::Lecture8()
{
	GLuint Shader = m_Lecture8Shader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);


	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, g_Time);

	GLfloat points[] = { 0, 0, 0.5, 0.5, 0.3, 0.3, -0.2, -0.2, -0.3, 0.3 };

	GLuint uPoints = glGetUniformLocation(Shader, "u_Points");
	glUniform2fv(uPoints, 5, points);

	g_Time += 0.005f;

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aUV = glGetAttribLocation(Shader, "a_UV");

	// 이거 빼먹어서 안됬었음
	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aUV);



	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGenRandRects);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) *5, 0);
	glVertexAttribPointer(aUV, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aUV);


}

void Renderer::Lecture9_Sin_VBOs()
{
	float size = 1.f;

	float texRect[]
		=
	{
		-size, size, 0.0f, 0.0f, 1.0f,	// X, Y, Z, U, V
		-size, -size, 0.0f , 0.0f, 0.0f,
		size, size, 0.0f , 1.0f, 1.0f,

		size, size, 0.0f , 1.0f, 1.0f,
		-size, -size, 0.0f , 0.0f, 0.0f,
		size, -size, 0.0f , 1.0f, 0.0f
	};

	glGenBuffers(1, &m_VBOTextureRect);	// Generate
	// 배열을 사용할 경우, GL_ARRAY_BUFFER를 써줘야 한다.
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(texRect), texRect, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.
}

void Renderer::Lecture9_Sin()
{
	GLuint Shader = m_SinShader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.1f;
	

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTexPos = glGetAttribLocation(Shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTexPos);



	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTexPos, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTexPos);
}


void Renderer::Lecture10_CheckerBoard_VBOs()
{

	float size = 0.5f;
	float rect[]
		=
	{
		-size, -size, 0.f, 0.f, 0.f,
		size, -size, 0.f, 1.f, 0.f,
		size, size, 0.f, 1.f, 1.f,//Triangle1 (X, Y, Z, TexPosX, TexPosY)

		-size, -size, 0.f, 0.f, 0.f,
		-size, size, 0.f, 0.f, 1.f,
		size, size, 0.f, 1.f, 1.f//Triangle2
	};

	glGenBuffers(1, &m_VBOTextureRect);	// Generate
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.


	static const GLulong checkerboard[] =
	{
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF
	};

	glGenTextures(1, &m_CheckerboardTexture);
	glBindTexture(GL_TEXTURE_2D, m_CheckerboardTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkerboard);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	
}

void Renderer::Lecture10_CheckerBoard()
{
	GLuint Shader = m_TextureSampleShader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;
	
	GLuint uTex = glGetUniformLocation(Shader, "u_Texture");
	glUniform1i(uTex, 0);
	glActiveTexture(GL_TEXTURE0);
	// 텍스쳐 여기서 변경
	glBindTexture(GL_TEXTURE_2D, m_RGBTexture);

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTexPos = glGetAttribLocation(Shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTexPos);



	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTexPos, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTexPos);
}

void Renderer::Lecture8_MultipleTextures_VBOs()
{

	float size = 0.5f;
	float rect[]
		=
	{
		-size, -size, 0.f, 0.f, 0.f,
		size, -size, 0.f, 1.f, 0.f,
		size, size, 0.f, 1.f, 1.f,//Triangle1 (X, Y, Z, TexPosX, TexPosY)

		-size, -size, 0.f, 0.f, 0.f,
		-size, size, 0.f, 0.f, 1.f,
		size, size, 0.f, 1.f, 1.f//Triangle2
	};

	glGenBuffers(1, &m_VBOTextureRect);	// Generate
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.
	/*
	static const GLulong checkerboard[] =
	{
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF
	};
	
	

	
	glGenTextures(1, &m_CheckerboardTexture);
	glBindTexture(GL_TEXTURE_2D, m_CheckerboardTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkerboard);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	*/

}

void Renderer::Lecture8_MultipleTextures()
{
	GLuint Shader = m_TextureSampleShader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;

	GLuint uTex[4];

	uTex[0] = glGetUniformLocation(Shader, "u_Texture");
	glUniform1i(uTex[0], 0);

	uTex[1] = glGetUniformLocation(Shader, "u_Texture1");
	glUniform1i(uTex[1], 1);

	uTex[2] = glGetUniformLocation(Shader, "u_Texture2");
	glUniform1i(uTex[2], 2);

	uTex[3] = glGetUniformLocation(Shader, "u_Texture3");
	glUniform1i(uTex[3], 3);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_CheckerboardTexture);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[0]);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[1]);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[2]);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[3]);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[4]);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, m_WoodTexture[5]);

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTexPos = glGetAttribLocation(Shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTexPos);



	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTexPos, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTexPos);
}

void Renderer::Lecture8_SingleTexture_VBOs()
{

	float size = 0.5f;
	float rect[]
		=
	{
		-size, -size, 0.f, 0.f, 0.f,
		size, -size, 0.f, 1.f, 0.f,
		size, size, 0.f, 1.f, 1.f,//Triangle1 (X, Y, Z, TexPosX, TexPosY)

		-size, -size, 0.f, 0.f, 0.f,
		-size, size, 0.f, 0.f, 1.f,
		size, size, 0.f, 1.f, 1.f//Triangle2
	};

	static const GLulong checkerboard[] =
	{
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF,
	0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000,
	0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF, 0x00000000, 0xFFFFFFFF
	};

	glGenBuffers(1, &m_VBOTextureRect);	// Generate
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);	// Bind
	glBufferData(GL_ARRAY_BUFFER, sizeof(rect), rect, GL_STATIC_DRAW);	// 실제 버퍼에 데이터를 넣는다.


	glGenTextures(1, &m_CheckerboardTexture);
	glBindTexture(GL_TEXTURE_2D, m_CheckerboardTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 8, 8, 0, GL_RGBA, GL_UNSIGNED_BYTE, checkerboard);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);


}

void Renderer::Lecture8_SingleTexture()
{
	GLuint Shader = m_TextureSampleShader;

	glUseProgram(Shader);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01f;

	GLuint uTex;

	uTex = glGetUniformLocation(Shader, "u_Texture");

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_ZeroToTen);

	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTexPos = glGetAttribLocation(Shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTexPos);


	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTexPos, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));


	glDrawArrays(GL_TRIANGLES, 0, 6);
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTexPos);
}

void Renderer::DrawNumber(int* number)
{
	GLuint Shader = m_DrawNumberShader;

	glUseProgram(Shader);

	// Uniform Inputs
	//GLuint uNumber = glGetUniformLocation(Shader, "u_Number");
	//glUniform1i(uNumber, number);
	GLuint uNumbers = glGetUniformLocation(Shader, "u_Numbers");
	glUniform1iv(uNumbers, 3, number);

	// Vertex Settings
	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTex = glGetAttribLocation(Shader, "a_Tex");
	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTex);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));

	// Texture Settings
	GLuint uTex = glGetUniformLocation(Shader, "u_Texture");
	glUniform1i(uTex, 0);	// 0번째 텍스쳐 슬롯을 사용하겠다.
	glActiveTexture(GL_TEXTURE0);	// 슬롯 활성화
	glBindTexture(GL_TEXTURE_2D, m_ZeroToTen);	// 슬롯에 사용할 텍스쳐를 바인딩

	// Draw Here
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Restore to default
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTex);

}

void Renderer::Lecture9_SpriteAnimTexture_VBOs(GLuint number)
{
	GLuint Shader = m_SpriteAnimShader;

	glUseProgram(Shader);

	Lecture10_CheckerBoard_VBOs();

	GLuint uNumber = glGetUniformLocation(Shader, "u_Number");
	glUniform1f(uNumber, (float)number);
	GLuint uX = glGetUniformLocation(Shader, "u_ResolX");
	glUniform1f(uX, (float)7);
	GLuint uY = glGetUniformLocation(Shader, "u_ResolY");
	glUniform1f(uY, (float)4);



	// Vertex Settings
	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	GLuint aTex = glGetAttribLocation(Shader, "a_TexPos");
	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTex);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));

	// Texture Settings
	GLuint uTex = glGetUniformLocation(Shader, "u_Texture");
	glUniform1i(uTex, 0);	// 0번째 텍스쳐 슬롯을 사용하겠다.
	glActiveTexture(GL_TEXTURE0);	// 슬롯 활성화
	glBindTexture(GL_TEXTURE_2D, m_SpriteAnimTexture);	// 슬롯에 사용할 텍스쳐를 바인딩

	// Draw Here
	glDrawArrays(GL_TRIANGLES, 0, 6);

	// Restore to default
	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTex);

}

void Renderer::VSSandbox()
{
	
	GLuint Shader = m_SandboxShader;
	glUseProgram(Shader);

	static float gTime = 0; gTime += 0.05f;
	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, gTime);

	GLfloat points[] = { 0, 0, 0.5, 0.5, 0.3, 0.3, -0.2, -0.2, -0.3, 0.3 };

	GLuint uPoints = glGetUniformLocation(Shader, "u_Points");
	glUniform2fv(uPoints, 5, points);

	GLuint aPos = glGetAttribLocation(m_SandboxShader, "a_Position");
	glEnableVertexAttribArray(aPos);

	GLuint uTex = glGetUniformLocation(m_SandboxShader, "u_Texture");
	glUniform1i(uTex, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_FlagTexture);


	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGridMesh);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_LINE_STRIP, 0, m_VBOGridMesh_Count);

	glDisableVertexAttribArray(aPos);

}

void Renderer::CubeVBO()
{
	float temp = 0.5f;

	float cube[] = {
	-temp,-temp,-temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, //x, y, z, nx, ny, nz, r, g, b, a
	-temp,-temp, temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	-temp, temp, temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,

	temp, temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,
	-temp,-temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,
	-temp, temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,

	temp,-temp, temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp,-temp,-temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	temp,-temp,-temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,

	temp, temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,
	temp,-temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,
	-temp,-temp,-temp, 0.f, 0.f, -1.f, 0.f, 0.f, 1.f, 1.f,

	-temp,-temp,-temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	-temp, temp, temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	-temp, temp,-temp, -1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,

	temp,-temp, temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp,-temp, temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp,-temp,-temp, 0.f, -1.f, 0.f, 0.f, 1.f, 0.f, 1.f,

	-temp, temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,
	-temp,-temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,
	temp,-temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,

	temp, temp, temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	temp,-temp,-temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	temp, temp,-temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,

	temp,-temp,-temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	temp, temp, temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,
	temp,-temp, temp, 1.f, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f,

	temp, temp, temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	temp, temp,-temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp, temp,-temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,

	temp, temp, temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp, temp,-temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,
	-temp, temp, temp, 0.f, 1.f, 0.f, 0.f, 1.f, 0.f, 1.f,

	temp, temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,
	-temp, temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,
	temp,-temp, temp, 0.f, 0.f, 1.f, 0.f, 0.f, 1.f, 1.f,
	};

	glGenBuffers(1, &m_VBO_Cube);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBO_Cube);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cube), cube, GL_STATIC_DRAW);

}

void Renderer::InitMatricies()
{
	//Calc ortho projection matrix (직교 투영 시키는 부분)
	m_OrthoProjMat4 = glm::ortho(-1.f, 1.f, -1.f, 1.f, 0.f, 2.f);
	m_PersProjMat4 = glm::perspective(3.141592f * 0.5f, 1.f, 0.001f, 100.f);	// 90도

	//Calc view Matrix (좌표계가 카메라 기준으로 바뀐다)
	// 직교 투영
	//m_CameraPosVec3 = glm::vec3(0.f, -1.f, 0.f);
	// 원근 투영
	m_CameraPosVec3 = glm::vec3(0.f, 1.f, 0.2f);

	// 직교 투영
	//m_CameraUpVec3 = glm::vec3(0.f, 0.f, 1.f);
	// 원근 투영
	m_CameraUpVec3 = glm::vec3(0.f, -1.f, 0.f);

	m_CameraLookatVec3 = glm::vec3(0.f, 0.f, 0.f);
	m_ViewMat4 = glm::lookAt(m_CameraPosVec3, m_CameraLookatVec3, m_CameraUpVec3);

	// 직교 투영
	//m_ViewProjMat4 = m_OrthoProjMat4 * m_ViewMat4;
	// 원근 투영
	m_ViewProjMat4 = m_PersProjMat4 * m_ViewMat4;
}

void Renderer::Cube()
{
	//CubeVBO();
	InitMatricies();
	CreateGridMesh();
	GLuint shader = m_Proj_Shader;

	glUseProgram(shader);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	GLfloat points[] = { 0, 0, 0.5, 0.5, 0.3, 0.3, -0.2, -0.2, -0.3, 0.3 };
	GLuint uPoints = glGetUniformLocation(shader, "u_Points");
	glUniform2fv(uPoints, 5, points);

	static float gTime = 0; gTime += 0.05f;
	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, gTime);

	GLuint uViewProjMat = glGetUniformLocation(shader, "u_ViewProjMat");
	glUniformMatrix4fv(uViewProjMat, 1, GL_FALSE, &m_ViewProjMat4[0][0]);
	
	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	glEnableVertexAttribArray(aPos);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGridMesh);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);

	glDrawArrays(GL_LINE_STRIP, 0, m_VBOGridMesh_Count);

	glDisableVertexAttribArray(aPos);
	

	// 큐브 사용할 때
	/*
	int attribPosition = glGetAttribLocation(shader, "a_Position");
	int attribNormal = glGetAttribLocation(shader, "a_Normal");
	int attribColor = glGetAttribLocation(shader, "a_Color");

	glEnableVertexAttribArray(attribPosition);
	glEnableVertexAttribArray(attribNormal);
	glEnableVertexAttribArray(attribColor);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBO_Cube);

	glVertexAttribPointer(attribPosition, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 10, 0);
	glVertexAttribPointer(attribNormal, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 10, (GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(attribColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 10, (GLvoid*)(sizeof(float) * 6));

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glDisableVertexAttribArray(attribPosition);
	glDisableVertexAttribArray(attribNormal);
	glDisableVertexAttribArray(attribColor);
	*/
}



void Renderer::Heightmap()
{
	GLuint Shader = m_HeightmapShader;

	glUseProgram(Shader);


	// 뒷면 제거
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);

	static float gTime = 0; gTime += 0.0005f;
	GLuint uTime = glGetUniformLocation(Shader, "u_Time");
	glUniform1f(uTime, gTime);

	GLuint uViewProjMat = glGetUniformLocation(Shader, "u_ViewProjMat");
	glUniformMatrix4fv(uViewProjMat, 1, GL_FALSE, &m_ViewProjMat4[0][0]);

	GLfloat points[] = { 0, 0, 0.5, 0.5, 0.3, 0.3, -0.2, -0.2, -0.3, 0.3 };

	GLuint uPoints = glGetUniformLocation(Shader, "u_Points");
	glUniform2fv(uPoints, 5, points);

	// Vertex Settings
	GLuint aPos = glGetAttribLocation(Shader, "a_Position");
	glEnableVertexAttribArray(aPos);
	glBindBuffer(GL_ARRAY_BUFFER, m_VBOGridMesh);
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, 0);


	// Texture Settings
	GLuint uHeightMapTex = glGetUniformLocation(Shader, "u_HeightMapTexture");
	glUniform1i(uHeightMapTex, 0);	// 0번째 텍스쳐 슬롯을 사용하겠다.
	GLuint uSnowTex = glGetUniformLocation(Shader, "u_SnowTexture");
	glUniform1i(uSnowTex, 1);	
	GLuint uGrassTex = glGetUniformLocation(Shader, "u_GrassTexture");
	glUniform1i(uGrassTex, 2);	

	glActiveTexture(GL_TEXTURE0);	// 슬롯 활성화
	glBindTexture(GL_TEXTURE_2D, m_HeightmapTexture);	// 슬롯에 사용할 텍스쳐를 바인딩
	glActiveTexture(GL_TEXTURE1);	
	glBindTexture(GL_TEXTURE_2D, m_SnowTexture);
	glActiveTexture(GL_TEXTURE2);	
	glBindTexture(GL_TEXTURE_2D, m_GrassTexture);	

	// Draw Here
	glDrawArrays(GL_TRIANGLES, 0, m_VBOGridMesh_Count);

	// Restore to default
	glDisableVertexAttribArray(aPos);

}

void Renderer::DrawTextureRect(GLuint tex, GLfloat x, GLfloat y, GLfloat sX, GLfloat sY)
{
	GLuint shader = m_DrawTextureRectShader;

	glUseProgram(shader);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01;
	GLuint uTexture = glGetUniformLocation(shader, "u_Texture");
	glUniform1i(uTexture, 0);
	GLuint uPos = glGetUniformLocation(shader, "u_Pos");
	GLuint uSize = glGetUniformLocation(shader, "u_Size");
	glUniform2f(uPos, x, y);
	glUniform2f(uSize, sX, sY);

	//BindNumberTextures();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aTex = glGetAttribLocation(shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTex);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);

	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTex);

}

GLuint Renderer::CreateFBO(int sx, int sy, GLuint *tex, bool isHDR)
{
	//Gen render target
	GLuint tempTex = 0;
	glGenTextures(1, &tempTex);
	glBindTexture(GL_TEXTURE_2D, tempTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
	if (isHDR)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, sx, sy, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

	}
	else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, sx, sy, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
	}
	*tex = tempTex;

	//Gen depth buffer
	glGenRenderbuffers(1, &m_DepthRenderBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, m_DepthRenderBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, sx, sy);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	// FBO 생성
	GLuint tempFBO;
	glGenFramebuffers(1, &tempFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, tempFBO);

	// FBO에 내용물 채우기
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tempTex, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_DepthRenderBuffer);

	GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (status != GL_FRAMEBUFFER_COMPLETE)
	{
		std::cout << "Error while attach fbo. \n";
		return 0;
	}
	
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return tempFBO;
}

void Renderer::DrawHDRTextureRect(GLuint tex, GLfloat x, GLfloat y, GLfloat sX, GLfloat sY)
{
	GLuint shader = m_HDRTextureRectShader;

	glUseProgram(shader);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01;
	GLuint uTexture = glGetUniformLocation(shader, "u_Texture");
	glUniform1i(uTexture, 0);
	GLuint uPos = glGetUniformLocation(shader, "u_Pos");
	GLuint uSize = glGetUniformLocation(shader, "u_Size");
	glUniform2f(uPos, x, y);
	glUniform2f(uSize, sX, sY);
	//GLuint uBlurSize = glGetUniformLocation(shader, "u_BlurSize");
	//GLuint uTexelSize = glGetUniformLocation(shader, "u_TexelSize");
	//glUniform1f(uBlurSize, 50);
	//glUniform2f(uTexelSize, 1.f / 1024, 1.f / 1024);

	//BindNumberTextures();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex);

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aTex = glGetAttribLocation(shader, "a_TexPos");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aTex);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOTextureRect);

	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 5, 0);
	glVertexAttribPointer(aTex, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 5, (GLvoid*)(sizeof(float) * 3));

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aTex);


}
void Renderer::ParticleForBloom()
{
	// TODO: 여기에 구현 코드 추가.
	GLuint shader = m_ParticleAnimShader;

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glUseProgram(shader);

	GLuint uTime = glGetUniformLocation(shader, "u_Time");
	glUniform1f(uTime, g_Time);
	g_Time += 0.01;


	GLuint uTex = glGetUniformLocation(shader, "u_Texture");
	glUniform1i(uTex, 0);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_Particle1Texture);

	GLuint aPos = glGetAttribLocation(shader, "a_Position");
	GLuint aVel = glGetAttribLocation(shader, "a_Vel");
	GLuint aSLRA = glGetAttribLocation(shader, "a_StartLifeRatioAmp");
	GLuint aRandV = glGetAttribLocation(shader, "a_RandV");
	GLuint aColor = glGetAttribLocation(shader, "a_Color");

	glEnableVertexAttribArray(aPos);
	glEnableVertexAttribArray(aVel);
	glEnableVertexAttribArray(aSLRA);
	glEnableVertexAttribArray(aRandV);
	glEnableVertexAttribArray(aColor);

	glBindBuffer(GL_ARRAY_BUFFER, m_VBOQuads1);

	//(0x, 1y, 2z, 3vx, 4vy, 5vz, 6s, 7l, 8r, 9a, 10v, 11x, 12y, 13z, 14vx
	glVertexAttribPointer(aPos, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15, 0);
	glVertexAttribPointer(aVel, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 15,
		(GLvoid*)(sizeof(float) * 3));
	glVertexAttribPointer(aSLRA, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15,
		(GLvoid*)(sizeof(float) * 6));
	glVertexAttribPointer(aRandV, 1, GL_FLOAT, GL_FALSE, sizeof(float) * 15,
		(GLvoid*)(sizeof(float) * 10));
	glVertexAttribPointer(aColor, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 15,
		(GLvoid*)(sizeof(float) * 11));

	glDrawArrays(GL_TRIANGLES, 0, m_VBOQuads_VertexCount1);

	glDisableVertexAttribArray(aPos);
	glDisableVertexAttribArray(aVel);
	glDisableVertexAttribArray(aSLRA);
	glDisableVertexAttribArray(aRandV);
	glDisableVertexAttribArray(aColor);

	glDisable(GL_BLEND);
}

void Renderer::TestFBO()
{
	glBindFramebuffer(GL_FRAMEBUFFER, m_FBO0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClearDepth(1.0f);
	glViewport(0, 0, 1024, 1024);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	ParticleForBloom();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.0, 0.0, 0.0, 0.0);
	glViewport(0, 0, 1024, 1024);
	DrawHDRTextureRect(m_FBOTexture0, 0.0, 0.0, 2.0, 2.0);

}

