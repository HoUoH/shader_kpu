#pragma once

#include <string>
#include <cstdlib>
#include <fstream>
#include <iostream>

#include "Dependencies\glew.h"
#include "Dependencies\wglew.h"
#include "Dependencies\glm/glm.hpp"
#include "Dependencies\glm/gtc/matrix_transform.hpp"
#include "Dependencies\glm/gtx/euler_angles.hpp"

class Renderer
{
public:
	Renderer(int windowSizeX, int windowSizeY);
	~Renderer();

	GLuint CreatePngTexture(char * filePath);
	GLuint CreateBmpTexture(char * filePath);

	void Test();
	void Lecture2();
	void GenRectsVBOs(int count);
	void Lecture3();
	void Lecture4();
	void Lecture5();
	void Lecture6();
	void Lecture6VBOs(int count, bool bRandPos);

	void Lecture7();
	void Lecture7VBOs(int count, bool bRandPos, bool bRandColor);

	void Lecture8();
	void Lecture8VBOs();

	void Lecture9_Sin();
	void Lecture9_Sin_VBOs();

	void Lecture10_CheckerBoard_VBOs();
	void Lecture10_CheckerBoard();

	void Lecture8_MultipleTextures_VBOs();
	void Lecture8_MultipleTextures();

	void Lecture8_SingleTexture_VBOs();
	void Lecture8_SingleTexture();
	void DrawNumber(int* number);

	void Lecture9_SpriteAnimTexture_VBOs(GLuint number);

	void VSSandbox();

	void CubeVBO();
	void Cube();
	void InitMatricies();

	void Heightmap();

	void DrawTextureRect(GLuint tex, GLfloat x, GLfloat y, GLfloat sX, GLfloat sY);
	GLuint CreateFBO(int sx, int sy, GLuint *tex, bool isHDR);
	void TestFBO();

	void DrawHDRTextureRect(GLuint tex, GLfloat x, GLfloat y, GLfloat sX, GLfloat sY);
	void GenQuadsVBO(int count, bool bRandPos, GLuint * id, GLuint * vCount);
	void ParticleForBloom();

	void FillAll(float alpha);


private:
	void Initialize(int windowSizeX, int windowSizeY);
	bool ReadFile(char* filename, std::string *target);
	void AddShader(GLuint ShaderProgram, const char* pShaderText, GLenum ShaderType);
	GLuint CompileShaders(char* filenameVS, char* filenameFS);
	void CreateVertexBufferObjects();

	void CreateGridMesh();

	unsigned char * Renderer::loadBMPRaw(const char * imagepath, unsigned int& outWidth, unsigned int& outHeight);

	bool m_Initialized = false;

	unsigned int m_WindowSizeX = 0;
	unsigned int m_WindowSizeY = 0;

	// VBOs
	GLuint m_VBORect = 0;
	GLuint m_VBOGenRandRects = 0;
	GLuint m_VBOSinTrail = 0;
	GLuint m_VBOTextureRect = 0;
	GLuint m_VBORectColor = 0;
	GLuint m_VBO_Cube = 0;
	GLuint m_VBOLecture = 0;
	GLuint m_VBOQuads = 0;

	// Matrices
	glm::mat4 m_ViewMat4;
	glm::mat4 m_PersProjMat4;
	glm::mat4 m_OrthoProjMat4;
	glm::mat4 m_ViewProjMat4;
	glm::vec3 m_CameraPosVec3;
	glm::vec3 m_CameraUpVec3;
	glm::vec3 m_CameraLookatVec3;

	// FBO
	GLuint m_FBO0 = 0;
	GLuint m_FBO1 = 0;
	GLuint m_FBO2 = 0;
	GLuint m_FBO3 = 0;



	int RectNum = 1000;
	//int VerticiesToRenderRect = RectNum * 6;
	int m_VBOGenRandRects_VertexCount = RectNum * 6;
	int m_sumOfVerticiesForQuads = 0;

	GLuint m_VBOQuads1 = 0;

	GLuint m_VBOQuads_VertexCount1 = 0;


	GLuint m_VBOGridMesh = 0;
	int m_VBOGridMesh_Count = 0;

	// Shaders
	GLuint m_SolidRectShader = 0;
	GLuint m_SimpleVelShader = 0;
	GLuint m_SinTrailShader = 0;
	GLuint m_Lecture8Shader = 0;
	GLuint m_FillAllShader = 0;
	GLuint m_SinShader = 0;
	GLuint m_TextureSampleShader = 0;
	GLuint m_DrawNumberShader = 0;
	GLuint m_SpriteAnimShader = 0;
	GLuint m_SandboxShader = 0;
	GLuint m_Proj_Shader = 0;
	GLuint m_HeightmapShader = 0;
	GLuint m_DrawTextureRectShader = 0;
	GLuint m_ParticleAnimShader = 0;
	GLuint m_HDRTextureRectShader = 0;

	// Texture IDs
	GLuint m_CheckerboardTexture = 0;
	GLuint m_CloudTexture = 0;
	GLuint m_PororoTexture = 0;
	GLuint m_RGBTexture = 0;
	GLuint m_SpriteAnimTexture = 0;
	GLuint m_FlagTexture = 0;
	GLuint m_HeightmapTexture = 0;
	GLuint m_SnowTexture = 0;
	GLuint m_GrassTexture = 0;
	GLuint m_FBOTexture0 = 0;
	GLuint m_FBOTexture1 = 0;
	GLuint m_FBOTexture2 = 0;
	GLuint m_FBOTexture3 = 0;
	GLuint m_Particle1Texture = 0;

	GLuint m_DepthRenderBuffer = 0;

	GLuint m_WoodTexture[9] = { 0 };

	GLuint m_ZeroToTen = 0;

};

