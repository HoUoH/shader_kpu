#version 450

uniform sampler2D u_Texture;
uniform sampler2D u_Texture1;
uniform sampler2D u_Texture2;
uniform sampler2D u_Texture3;



in vec2 v_TexPos;

out vec4 FragColor;

uniform float u_Time;

void main()
{
	// 텍스쳐는 좌표를 생성한 다음, 좌표를 기준으로
	// 해당 좌표에 해당하는 텍스쳐 좌표를 가져와서 샘플링하는 것 (갖다 붙이는게 아님)

	// 1. UV 출력
	/*
	vec4 newColor = texture(u_Texture, v_TexPos);
	newColor = vec4(v_TexPos, 0, 1);
	*/



	// 2. 텍스쳐 반복 출력(버텍스 좌표는 동일, TexPos는 0~1, 0~1 반복되게)
	/*
	vec2 newTexPos = fract(v_TexPos * 2);
	vec4 newColor = texture(u_Texture, newTexPos);
	*/
	


	// 3. 텍스쳐 이동 (Cos, Sin을 이용하면 원을 그리며 돌아간다)
	/*
	vec2 newTexPos = fract(v_TexPos * 3 + vec2(u_Time, sin(u_Time * 4)/2));
	vec4 newColor = texture(u_Texture, newTexPos);
	*/
	



	// 4. ★ 시험문제 ★
	/*
	vec2 newTexPos = abs(v_TexPos - vec2(0, 0.5)) + vec2(0, 0.5);

	vec4 newColor;
	newColor = texture(u_Texture, 1-newTexPos);
	*/

	// 5. 블러 효과 (텍스쳐 다섯개, 더한 후, 평균내기)
	// 잘 티가 안난다. 5개로는 불충분함.
	/*
	vec2 newTexPos[5];
	vec4 newColorArray[5];

	float width = 1.0/256.0;
	float height = 1.0/256.0;


	newTexPos[0] = v_TexPos;
	newTexPos[1] = vec2(v_TexPos.x - width, v_TexPos.y);
	newTexPos[2] = vec2(v_TexPos.x + width, v_TexPos.y);
	newTexPos[3] = vec2(v_TexPos.x, v_TexPos.y + height);
	newTexPos[4] = vec2(v_TexPos.x, v_TexPos.y - height);

	newColorArray[0] = texture(u_Texture, newTexPos[0]);
	newColorArray[1] = texture(u_Texture, newTexPos[1]);
	newColorArray[2] = texture(u_Texture, newTexPos[2]);
	newColorArray[3] = texture(u_Texture, newTexPos[3]);
	newColorArray[4] = texture(u_Texture, newTexPos[4]);

	vec4 newColor = newColorArray[0] + newColorArray[1] + newColorArray[2] + newColorArray[3] + newColorArray[4];

	newColor /= 5;
	*/

	// 6. Lecture6 PPT 34P의 두번째 그림
	/*
		vec2 newTexPos;

		// step 1. X좌표값을 0~1, 0~1, 0~1로 만들기
		//newTexPos.x = fract(v_TexPos.x * 3.0);
		//newTexPos.y = v_TexPos.y;
		
		// step 2. Y좌표값을 0~1/3로 만들기
		//newTexPos.x = fract(v_TexPos.x * 3.0);
		//newTexPos.y = v_TexPos.y/3;
		
		// step 3.  (1). Y값의 시작점은 0으로 동일하지만 '변화량, 증가하는 추세'은 1/3로 모두 동일하다.
		//			(2). 첫번째 영역에서 모든 프래그먼트의 값을 1, 두번째 영역에서 모든 프래그먼트의 값을 2, 3으로 만든다.
		//			(3). 그 다음, 원래 0~1로 증가하던 값을 3으로 나눠준다 (step2)
		//			(4). (3)을 (2)와 더한다.

		newTexPos.x = fract(v_TexPos.x * 3.0);
		newTexPos.y = v_TexPos.y / 3.0;	// 0~1/3
		newTexPos.y += floor(v_TexPos.x*3) / 3.0; // offset 0~0, 1~1, 2~2
	
	*/

	// 7. Lecture6 PPT 34P의 세번째 그림
	/*
	vec2 newTexPos;

	newTexPos.x = fract(v_TexPos.x * 3.0);
	newTexPos.y = v_TexPos.y / 3.0;
	newTexPos.y += (2-floor(v_TexPos.x*3)) / 3.0;	// offset 2~2, 1~1, 0~0
	*/

	// 8. Lecture6 PPT 34P의 네번째 그림

	// 변화량은 1/3로 동일, Y축 양의 방향으로
	// 오프셋은 아래서부터 위로 2/3, 1/3, 0
	/*
	vec2 newTexPos;
	newTexPos.x = v_TexPos.x;
	newTexPos.y = (2-floor(v_TexPos.y * 3.0)) / 3.0; // 아래에서 위로 2/3, 1/3, 0/3 offset
	newTexPos.y += fract(v_TexPos.y * 3.0) / 3.0;
	*/

	// 9. Lecture6 PPT 35P [[시험문제]]
	/*
	vec2 newTexPos;
	newTexPos = v_TexPos * 2;
	newTexPos.y = floor(newTexPos.y);
	*/

	// 다양한 텍스쳐를 사용하고 싶다면, uniform형 Texture가 여러개 있어야한다.
	// (외부에서 다른 u_Texture선언)
	// u_Texture는 외부에서 Bind되기 때문에, 내부에서 변경할 수 없다

	// 10. Lecutre 8 [ 다중 텍스쳐 사용]
	// 왼쪽편, 오른쪽편 다른 텍스쳐 배치하기
	/*
	vec2 newTexPos;
	newTexPos = vec2(v_TexPos.x, 1.0 - v_TexPos.y);

	vec4 newColor;

	
	if (newTexPos.x < 0.5)
	{
		newColor = texture(u_Texture, vec2(newTexPos.x*2, newTexPos.y));
	}
	else
	{
		newColor = texture(u_Texture1, vec2(fract(newTexPos.x*2), newTexPos.y));
	}
	*/

	// 11.  01
	//		23 으로 텍스쳐 출력하기
	
	/*
	vec2 newTexPos = vec2(v_TexPos.x, 1.0 - v_TexPos.y);
	vec4 newColor;
	if (newTexPos.x < 0.5 && newTexPos.y < 0.5)
	{
		newColor = texture(u_Texture, vec2(newTexPos.x*2, newTexPos.y*2));
	}
	else if (newTexPos.x > 0.5 && newTexPos.y < 0.5)
	{
		newColor = texture(u_Texture1, vec2(fract(newTexPos.x*2), newTexPos.y*2));
	}
	else if (newTexPos.x < 0.5 && newTexPos.y > 0.5)
	{
		newColor = texture(u_Texture2, vec2(newTexPos.x*2, fract(newTexPos.y*2)));
	}
	else if (newTexPos.x > 0.5 && newTexPos.y > 0.5)
	{
		newColor = texture(u_Texture3, vec2(fract(newTexPos.x*2), fract(newTexPos.y*2)));
	}
	*/


	
    //FragColor = newColor;
}
