#version 450

layout(location=0) out vec4 FragColor;

uniform sampler2D u_Texture;

in vec2 v_Tex;

//uniform int u_Number;	// 1 digit
uniform int u_Numbers[3];


void main()
{
// --------------------
	// 단일 텍스쳐
	// --------------------

	// 1. 세로로 긴 텍스쳐 한장을, 잘라서 입력값에 따라 해당 숫자를 그린다
	/*
	vec2 newTex = v_Tex;
	float newY = 9.0 - float(u_Number);	// 텍스쳐가 맨 아래가 9기 때문에 바꿔주려고

	newTex.y = newTex.y / 10.0 + newY / 10.0;
	vec4 newColor = texture(u_Texture, newTex, 1);
	*/


	// 2. 세 자리수를 입력 받으면 한 화면을 3등분 해서 하나씩 출력한다.
	vec2 newTex = v_Tex;

	// x_Tex Coord
	newTex.x = fract(newTex.x * 3.0);

	// 0~ 1일 경우 index가 0인 애를 그린다. (인덱스를 계산해 주는 부분 필요)
	int newIndex = int(floor(v_Tex.x *3.0));

	float newY = 9.0 - float(u_Numbers[newIndex]);
	newTex.y = newTex.y / 10.0 + newY / 10.0;
	vec4 newColor = texture(u_Texture, newTex);

	FragColor = newColor;
}
