#version 450
in vec3 v_Grey;

out vec4 FragColor;

uniform sampler2D u_Texture;

in vec2 v_Tex;

void main()
{
    FragColor = vec4(v_Grey, 1.0);
}
