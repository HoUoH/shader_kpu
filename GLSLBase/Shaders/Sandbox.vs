#version 450

in vec3 a_Position;

const float PI = 3.141592;

uniform float u_Time;
uniform vec2 u_Points[5];

out float v_Grey;
out vec2 v_Tex;



void Flag()
{
/*프래그먼트 셰이더를 이용하는 것 보다 훨씬 경제적임*/

	// 1. 진폭 높이에 따라 그레이 스케일 적용
	/*
	vec3 newPos = a_Position;

	// x :: -0.5~0.5 --> +0.5 -> 0~1 -> * 2 * PI -> 0~2PI

	float value = (newPos.x + 0.5) * 2 * PI + u_Time;
	float sinValue = sin(value) * 0.5;	// -0.5~ 0.5의 값을 가짐

	// y :: -0.5~0.5
	newPos.y = newPos.y + sinValue;

	v_Grey = sinValue + 0.5;	// 0 ~ 1의 값을 가짐

	*/

	// -------------------------------------

	// 2. 깃발 처럼, 첫 시작 부분 고정시키기
	// 버텍스셰이더만 바꾸면 된다.
	// '가중치'를 두어서 원점에서 멀수록 가중치가 0~1로 증가한다.
	/*
	vec3 newPos = a_Position;
	// 가중치 (0 ~ 1)
	float additionalValue = newPos.x + 0.5; // 0~1

	// 주기
	float period = 1.0 + additionalValue * 0.1;	
	
	

	float value = ((newPos.x + 0.5) * 2 * PI * period) - u_Time ;
	float sinValue = sin(value) * 0.2;	// value에 곱해지는게 주기

	newPos.y = newPos.y + sinValue * additionalValue;

	v_Grey = sinValue + 0.5;
	*/

	// -------------------------------------

	// 3. 끝부분 펄럭이게 (칼같이 안짤리게)
	
	vec3 newPos = a_Position;

	// 가중치 (0 ~ 1)
	float additionalValueX = newPos.x + 0.5; // 0~1
	float additionalValueY = newPos.y + 0.5;// 0~1

	// 주기
	float periodX = 1.0 + (1.0 - additionalValueY) * 0.5;	
	float periodY = 1.0 + additionalValueX * 0.5;
	
	float valueX = (additionalValueY * 2 * PI * periodX) - u_Time;
	float valueY = (additionalValueX * 2 * PI * periodY) - u_Time;

	float sinValueX = sin(valueX) * 0.08;
	float sinValueY = sin(valueY) * 0.2;

	// y Scale
	newPos.y = newPos.y * ((1.0 - additionalValueX) * 0.5 + 0.5);


	// x
	newPos.x = newPos.x - sinValueX * additionalValueX;

	// y
	newPos.y = newPos.y + sinValueY * additionalValueX;

	v_Grey = sinValueY + 0.5;
	v_Tex = vec2(0.5, 0.5) + a_Position.xy; // 0~1, 0~1 의 Tex Coordinate로 바뀜
	
	gl_Position = vec4(newPos, 1);
	
}

void Wave()
{
	// 1. 웨이브
	/*
	vec2 target;
	vec2 source;
	target = a_Position.xy;
	source = vec2(0.0, 0.0);

	float dis = length(target - source) * 4 * PI;	// 0 ~ 0.5 ==> 0 ~ 2PI
	float grey = sin(dis - u_Time);

	gl_Position = vec4(a_Position, 1);

	v_Grey = (grey + 1.0) / 2.0;
	v_Tex =  vec2(0.5, 0.5) + a_Position.xy;
	*/

	// 2. 웨이브에 점찍기
	float grey = 0.0;
	vec3 newPos = a_Position.xyz;

	float period = 2.0;
	for (int i = 0; i < 5; i++)
	{
		vec2 target;
		vec2 source;
		target = a_Position.xy;
		// 핵심라인
		source = u_Points[i];

		float dis = length(target - source) * 4 * PI * period;	// 0 ~ 0.5 ==> 0 ~ 2PI
		// 핵심라인 (grey = 에서 gery +=로)
		grey += sin(dis - u_Time);
	}

	// 3D 일 경우 이 라인이 추가됨, Clipping 해결하기 위해 * 0.05 한 것
	newPos.z += grey * 0.05;

	gl_Position = vec4(newPos, 1);

	v_Grey = (grey + 1.0) / 2.0;
	v_Tex =  vec2(0.5, 0.5) + a_Position.xy;

}

void SphereMapping()
{
	// 원래의 좌표를 바꿔서 출력하는 방법 (VertexShader의 특징을 이용)
	// LINESTRIP으로 그려야 구인지 확인하기 편하다

	vec3 newPos;

	vec3 originPos = a_Position;
	float newTime = fract(u_Time);

	float radius = 0.5;
	float beta = (a_Position.x + 0.5) * 2 * PI;		// -0.5~0.5 ==> 0 ~ 1 ==> 0PI ~ 2PI
	float theta = (a_Position.y + 0.5) * PI;	// 0~PI
	vec3 SpherePos = a_Position.xyz;

	// 구의 반지름 (p)
	SpherePos = vec3(
		radius * sin(theta) * cos(beta),
		radius * sin(theta) * sin(beta),
		radius * cos(theta)
	);

	// mix라고 함수가 따로 있다.
	// float interpol = fract(u_Time);
	// newPos = mix(originPos, spherePos, interpol)
	newPos = originPos * (1.0 - newTime) + SpherePos * (newTime);

	gl_Position = vec4(newPos, 1);
	v_Grey = 1;

}

void main()
{
	//Flag();
	//Wave();
	SphereMapping();
}
