#version 450

layout(location=0) out vec4 FragColor;

in vec4 v_Temp;

// 프래그먼트 셰이더로 바로 uniform 값을 넘길 수 있다.
uniform float u_Time;

uniform vec2 u_Points[5];

//const vec2 c_Points[2] = {vec2(0.0,0.0) , vec2(0.2, 0.2)};
const float PI = 3.141592;

void main()
{
	// 1. TexCoord처럼 나오게하기
	//FragColor = vec4(v_Temp.xy, 0 ,1);

	// 2. 
	/*
	float distance = sqrt(v_Temp.x * v_Temp.x + v_Temp.y * v_Temp.y);
	
	if (distance < 0.5f)
	{
		FragColor = vec4(1.f);
	}
	else
	{
		FragColor = vec4(0.f);
	}
	*/
	

	// 3. Sin을 이용해서 화면 중앙에 동심원 나오게
	/*
	vec2 newUV = v_Temp.xy - vec2(0.5, 0.5);
	float distance = sqrt(newUV.x * newUV.x + newUV.y * newUV.y);
	float grey = sin(distance * 3.141592 * 4 * 2);
	
	if (distance < 0.5f && distance > 0.48)
	{
		FragColor = vec4(grey);
	}
	else 
	{
		FragColor = vec4(grey);
	}
	*/


	// 4. 점 찍기
	/*
	vec2 newUV = v_Temp.xy - vec2(0.5, 0.5); // -0.5 ~ 0.5

	float pointGrey = 0;

	for (int i = 0; i < 2; i++)
	{
		vec2 newPoint = c_Points[i];
		vec2 newVec = newPoint - newUV;
		float distance = sqrt(newVec.x * newVec.x + newVec.y * newVec.y);

		if (distance < 0.1f)
		{
			pointGrey += 0.5f * pow((1-(distance)/0.1), 1);
		}
	}

	FragColor = vec4(pointGrey);
	*/
	
	// 5. sin이용해서 만든 원이 움직이도록
	vec2 newUV = v_Temp.xy - vec2(0.5, 0.5); // -0.5 ~ 0.5
	float pointGrey = 0;
	

	float distance = length(newUV);	// 루트 x^2 + y^2과 같음
	float newTime = fract(u_Time);
	float ringWidth = 0.1f;
	
	if (distance < newTime + ringWidth && distance > newTime)
	{

		float temp = (distance - newTime) / ringWidth;
		pointGrey = temp;

		// 링 영역 안에 있을 때만 Point를 밝게 해줘라
		for (int i = 0; i < 5; i++)
		{
			vec2 newPoint = u_Points[i];
			vec2 newVec = newPoint - newUV;
			float distance = length(newVec);
			if (distance < 0.1f)
			{
				pointGrey += 0.5f * pow((1-(distance)/0.1), 5);
			}
		}
	}
	

	FragColor = vec4(pointGrey);
}
