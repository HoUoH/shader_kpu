#version 450

in vec3 a_Position;
in vec3 a_Vel;
in vec2 a_StartLife;

uniform float u_elapsedTime;
uniform bool u_Repeat = true;

const vec3 c_Gravity = vec3(0, -5, 0);

// - 파티클 : 속력, 방향 (속도) 랜덤
// - 주어진 값들을 이용해서 움직이는 것을 어떻게 만들 수 있을까
// - lifetime을 넣으면, 전체 초기화 하는 느낌을 방지할 수 있다.
// - 버텍스마다 달라야 하는 값들을 만드는 것은, Vertex Shader안에서 할 수 없다.
//   VertexAttribLocation을 통해서 VertexShader에 넣어주어야 한다.

void main()
{
	// newPos : '초기 위치'가 입력된다.
	vec3 newPos = a_Position.xyz;
	float startTime = a_StartLife.x;
	float lifeTime = a_StartLife.y;

	float newTime = u_elapsedTime - startTime; // 생성되고 난 후 흐른 시간

	// 나중 위치 = 초기 위치 + 초기 속도 * 시간
	// 현재 속도 = 초기 속도 + 가속도 * 시간
	// ==> (중력 가속도가 더해졌을 때- 속도가 두개임 나중 위치) = 초기 위치 + (초기속도 * 시간) + (1/2 * a * t^2)
	// 초기 속도와 초기 위치를 알면, 변화하는 위치를 알 수 있다.

	if (newTime > 0) 
	{
		float life = newTime;

		float remainingLife = lifeTime - life;

		if (u_Repeat == true)
		{
			remainingLife = 1.f;
			newTime = mod(newTime, lifeTime);
		}

		if (remainingLife < 0) // 생존시간이 생성되고 난 후 흐른시간보다 클 경우 이상한데 그림
		{
			newPos = vec3(10000, 10000, 10000);
		}
		else
		{
			newPos += a_Vel * (newTime)+ 0.5 * c_Gravity * newTime * newTime;	// += 해야 기존 모양 유지가능
		}
	}
	else
	{
		newPos = vec3(10000, 10000, 10000);
	}
	
	gl_Position = vec4(newPos, 1);
}
