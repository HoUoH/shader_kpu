#version 450

in vec3 a_Position;
in vec2 a_Tex;

out vec2 v_Tex;

uniform float u_Time;

void main()
{
	float newTime = fract(u_Time);

    gl_Position = vec4(a_Position, 1.0);
    v_Tex = a_Tex;
}
