#version 450

out vec4 FragColor;

uniform sampler2D u_Texture;

in float v_Grey;
in vec2 v_Tex;

const float PI = 3.141592;

void main()
{
	//FragColor = texture(u_Texture, v_Tex)*v_Grey;
	FragColor = vec4(vec3(v_Grey),1);
}
