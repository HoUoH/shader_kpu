#version 450

uniform sampler2D u_Texture;

in vec2 v_TexPos;

out vec4 FragColor;

uniform float u_Number;
uniform float u_ResolX;
uniform float u_ResolY;

void main()
{
	
	vec2 newTexPos;

	float x_index = fract(u_Number / u_ResolX) * u_ResolX;
	float y_index = floor(u_Number / u_ResolX);
	//int y_index = 1;

	newTexPos.x = v_TexPos.x/float(u_ResolX) + x_index/float(u_ResolX);
	newTexPos.y = v_TexPos.y/float(u_ResolY) + y_index/float(u_ResolY);
	

	//vec2 newTexPos = v_TexPos;

	vec4 newColor = texture(u_Texture, newTexPos);
    FragColor = newColor;
}
