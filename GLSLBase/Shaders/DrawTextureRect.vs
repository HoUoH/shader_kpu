#version 450

in vec3 a_Position;
in vec2 a_TexPos;

uniform vec2 u_Pos;
uniform vec2 u_Size;

out vec2 v_TexPos;

void main()
{
	vec3 newPos = vec3(a_Position.xy * u_Size.xy + u_Pos.xy, 0);
	gl_Position = vec4(newPos.xyz, 1);

	v_TexPos = a_TexPos;
}
