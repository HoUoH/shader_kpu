#version 450

in vec3 a_Position;
//in vec3 a_Normal;
//in vec4 a_Color;


const float PI = 3.141592;
uniform float u_Time;
uniform vec2 u_Points[5];
uniform mat4 u_ViewProjMat;
out vec3 v_Grey;

void Flag()
{
	// 3. 끝부분 펄럭이게 (칼같이 안짤리게)
	
	vec3 newPos = a_Position;

	// 가중치 (0 ~ 1)
	float additionalValueX = newPos.x + 0.5; // 0~1
	float additionalValueY = newPos.y + 0.5;// 0~1

	// 주기
	float periodX = 1.0 + (1.0 - additionalValueY) * 0.5;	
	float periodY = 1.0 + additionalValueX * 0.5;
	
	float valueX = (additionalValueY * 2 * PI * periodX) - u_Time;
	float valueY = (additionalValueX * 2 * PI * periodY) - u_Time;

	float sinValueX = sin(valueX) * 0.08;
	float sinValueY = sin(valueY) * 0.2;

	// y Scale
	newPos.y = newPos.y * ((1.0 - additionalValueX) * 0.5 + 0.5);

	// x
	newPos.x = newPos.x - sinValueX * additionalValueX;

	// y
	newPos.y = newPos.y + sinValueY * additionalValueX;

	v_Grey = vec3(sinValueY + 0.5);
	
	//gl_Position = vec4(newPos, 1);
	gl_Position = u_ViewProjMat * vec4(newPos, 1.0);
	
}

void SphereMapping()
{
	// 원래의 좌표를 바꿔서 출력하는 방법 (VertexShader의 특징을 이용)
	// LINESTRIP으로 그려야 구인지 확인하기 편하다

	vec3 newPos;

	vec3 originPos = a_Position;
	float newTime = fract(u_Time);

	float radius = 0.5;
	float beta = (a_Position.x + 0.5) * 2 * PI;		// -0.5~0.5 ==> 0 ~ 1 ==> 0PI ~ 2PI
	float theta = (a_Position.y + 0.5) * PI;	// 0~PI
	vec3 SpherePos = a_Position.xyz;

	// 구의 반지름 (p)
	SpherePos = vec3(
		radius * sin(theta) * cos(beta),
		radius * sin(theta) * sin(beta),
		radius * cos(theta)
	);

	// mix라고 함수가 따로 있다.
	// float interpol = fract(u_Time);
	// newPos = mix(originPos, spherePos, interpol)
	newPos = originPos * (1.0 - newTime) + SpherePos * (newTime);

	gl_Position = u_ViewProjMat * vec4(newPos, 1.0);
	v_Grey = vec3(1.0);

}

void Wave()
{
	// 1. 웨이브
	/*
	vec2 target;
	vec2 source;
	target = a_Position.xy;
	source = vec2(0.0, 0.0);

	float dis = length(target - source) * 4 * PI;	// 0 ~ 0.5 ==> 0 ~ 2PI
	float grey = sin(dis - u_Time);

	gl_Position = vec4(a_Position, 1);

	v_Grey = (grey + 1.0) / 2.0;
	v_Tex =  vec2(0.5, 0.5) + a_Position.xy;
	*/

	// 2. 웨이브에 점찍기
	float grey = 0.0;
	vec3 newPos = a_Position.xyz;

	float period = 2.0;
	for (int i = 0; i < 5; i++)
	{
		vec2 target;
		vec2 source;
		target = a_Position.xy;
		// 핵심라인
		source = u_Points[i];

		float dis = length(target - source) * 4 * PI * period;	// 0 ~ 0.5 ==> 0 ~ 2PI
		// 핵심라인 (grey = 에서 gery +=로)
		grey += sin(dis - u_Time);
	}

	// 3D 일 경우 이 라인이 추가됨, Clipping 해결하기 위해 * 0.05 한 것
	newPos.z += grey * 0.05;

	gl_Position = u_ViewProjMat * vec4(newPos, 1.0);

	//v_Grey = vec3((grey + 1.0) / 2.0);
	v_Grey = vec3(1.0);

	//v_Tex =  vec2(0.5, 0.5) + a_Position.xy;

}

void Proj()
{
	gl_Position = u_ViewProjMat * vec4(a_Position, 1.0);
	v_Grey = vec3(1.0);
}

void main()
{
	//Proj();
	//Flag();
	//SphereMapping();
	Wave();
}
