#version 450

layout(location=0) out vec4 FragColor;


in float v_Grey;
in vec2 v_Tex;
in vec2 v_Temp;
in vec3 v_Normal;
in vec3 v_Pos;	// 현재 나의 위치


uniform sampler2D u_SnowTexture;
uniform sampler2D u_GrassTexture;
uniform float u_Time;

const vec3 c_Light1 = vec3(0.0, -1.0, 0.3);	// 카메라 위치를 PointLight 위치로.
const vec3 c_CameraDir = vec3(0.0, -1.0, 0.2);


void Terrain()
{
   vec4 snowColor = texture(u_SnowTexture, v_Tex) * v_Grey;
   vec4 grassColor = texture(u_GrassTexture, v_Tex) * (1.0 - v_Grey);
   vec4 finalColor = snowColor + grassColor;

   // -------------------------------------
   FragColor = finalColor;

}

void TerrainWithLight()
{
   vec4 snowColor = texture(u_SnowTexture, v_Tex) * v_Grey;
   vec4 grassColor = texture(u_GrassTexture, v_Tex) * (1.0 - v_Grey);
   vec4 finalColor = snowColor + grassColor;
   
   float aIntensity = 0.4;	// ambient
   float dIntensity = 0.5;	// diffuse
   float sIntensity = 1.0;	// specular

   vec3 lightDir = c_Light1 - v_Pos;	// 발표자료의 식의 L에 해당.
   vec3 ambient = vec3(1, 1, 1);
   float diffuse = clamp(dot(lightDir, v_Normal), 0.0, 1.0);
   vec3 diffuseColor = vec3(1, 1, 1);
   vec3 reflectDir = reflect(lightDir, v_Normal);
   // V는 현재 버텍스에서 카메라를 바라보는 벡터를 의미.
   vec3 viewDir = v_Pos - c_CameraDir;
   vec3 specColor = vec3(1, 0, 0);
   float spec = clamp(dot(viewDir, reflectDir), 0.0, 1.0);
   spec = pow(spec, 6.0);

   vec3 newColor = ambient * aIntensity + diffuseColor*diffuse*dIntensity + specColor*spec*sIntensity;
   
   // -------------------------------------
	FragColor = vec4(newColor, 1);
}


void main()
{
	//Terrain();
	TerrainWithLight();
}
