#version 450

layout(location=0) out vec4 FragColor;

uniform sampler2D u_Texture;

in vec4 v_Color;
in vec2 v_OriXY;
in float v_Radius;
in vec2 v_Tex;

uniform float u_Time;

uniform float u_BlurSize = 50;
uniform vec2 u_TexelSize = vec2(1.0/1024.0, 1.0/1024.0);

void main()
{
	
	float size = u_BlurSize / 2.0;
	vec2 xDir = vec2(1.0, 0.0);
	vec2 yDir = vec2(0.0, 1.0);
	vec3 newColor = vec3(0.0, 0.0, 0.0);
	float count = 0.0;
	float maxDis = length(size*vec2(1.0, 0.0) * u_TexelSize);

	for (float x = -size; x < size; x += 1.0)
	{
		for (float y = -size; y <size; y += 1.0)
		{
			vec2 newTex = v_Tex + (x * xDir * u_TexelSize) + (y * yDir * u_TexelSize);

			float dis = length(newTex - v_Tex);
			float add = clamp(1.0 - dis / maxDis, 0.0, 1.0);	// 가중치 (원형태로 블러하기위해)
			add = pow(add, 5);

			vec4 temp = texture(u_Texture, newTex);
			temp = clamp(temp - vec4(1.0), 0.0, 100.0);
			newColor += temp.rgb * add;

			//newColor += texture(u_Texture, newTex).rgb;
			count += 0.1;
		}
	}


	FragColor = vec4(newColor/count + texture(u_Texture, v_Tex).rgb, 1.0);
	
	//vec2 newTex = v_Tex;
	//vec4 newColor = texture(u_Texture, newTex);
	//FragColor = newColor;
}
