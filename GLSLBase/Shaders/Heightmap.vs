#version 450

in vec3 a_Position;
in vec2 a_TexPos;
uniform float u_Time;

out float v_Grey;
out vec2 v_Temp;
out vec2 v_Tex;
out vec3 v_Normal;
out vec3 v_Pos;
uniform mat4 u_ViewProjMat;

uniform sampler2D u_HeightMapTexture;
uniform sampler2D u_SnowTexture;
uniform sampler2D u_GrassTexture;

const float PI = 3.141592;

uniform vec2 u_Points[5];


void Wave()
{
	float grey = 0.0;
	vec3 newPos = a_Position.xyz;

	float period = 6;
	for (int i = 0; i < 5; i++)
	{
		vec2 target;
		vec2 source;
		target = a_Position.xy;
		// 핵심라인
		source = u_Points[i];

		float dis = length(target - source) * 4 * PI * period;	// 0 ~ 0.5 ==> 0 ~ 2PI
		// 핵심라인 (grey = 에서 gery +=로)
		grey += sin(dis - u_Time);
	}

	// 3D 일 경우 이 라인이 추가됨, Clipping 해결하기 위해 * 0.05 한 것
	newPos.z += grey * 0.005;

	gl_Position = u_ViewProjMat * vec4(newPos, 1.0);;

	v_Grey = (grey + 1.0) / 4.0;
	v_Tex =  vec2(0.5, 0.5) + a_Position.xy;
}

void HeightMap()
{
	float gap = 2.0/200.0;
	float HeightThres = 0.3;

	vec2 newUV = a_Position.xy + vec2(0.5 + u_Time, 0.5);
	vec2 newUVRight = a_Position.xy + vec2(0.5 + u_Time, 0.5) + vec2(gap, 0.0); // 0~1
	vec2 newUVUp = a_Position.xy + vec2(0.5 + u_Time, 0.5) + vec2(0.0, gap); // 0~1

	float height = texture(u_HeightMapTexture, newUV).r;
	float heightRight = texture(u_HeightMapTexture, newUVRight).r;
	float heightUp = texture(u_HeightMapTexture, newUVUp).r;


	vec3 newPos = vec3(a_Position.xy, a_Position.z + height * HeightThres);
	vec3 newPosRight = vec3(a_Position.xy + vec2(gap, 0), a_Position.z + heightRight * HeightThres);
	vec3 newPosUp = vec3(a_Position.xy + vec2(0, gap), a_Position.z + heightUp * HeightThres);

	// 두개를 CrossProduct 해서 Normal벡터 만들기
	vec3 diff1 = newPosRight - newPos;
	vec3 diff2 = newPosUp - newPos;

	vec3 normal = cross(diff1, diff2);


	gl_Position = u_ViewProjMat * vec4(newPos, 1.0);
	v_Grey = height;
	v_Tex = newUV;
	v_Normal = normalize(normal);
	v_Pos = newPos;
}

void Proj()
{
	gl_Position = u_ViewProjMat * vec4(a_Position, 1.0);
    v_Grey = 1.0;

}

void main()
{
	//Proj();
	HeightMap();
	//Wave();
}
