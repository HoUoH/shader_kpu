#version 450

layout(location=0) out vec4 FragColor;

in vec2 v_TexPos;

uniform float u_Time;

const float PI = 3.141592;

void main()
{
	vec4 newColor = vec4(0);
	float newTime = u_Time;


	// 1. Sin함수 위는 흰색, 아래는 하얀색
	// v_Tex : x 0~1 --> 0~2 PI
	/*
	float x = v_TexPos.x * 2 * PI;
	float sinResult = (sin(x)+1) / 2;
	if ( sinResult > v_TexPos.y)
	{
		newColor = vec4(1);
	}
	*/

	// 2. Sin 곡선의 라인만 나오도록
	/*
	float x;
	x = v_TexPos.x * 2 * PI;
	float sinResult = (sin(x) + 1) / 2;
	float thickness = 0.01f;
	if ( sinResult > v_TexPos.y && sinResult - thickness < v_TexPos.y)
	{
		newColor = vec4(1);
	}
	*/

	// 3. Sin 곡선이 움직이도록
	/*
	float x;
	x += v_TexPos.x * 2 * PI + newTime;
	float sinResult = (sin(x) + 1) / 2;
	float thickness = 0.01f;
	if ( sinResult > v_TexPos.y && sinResult - thickness < v_TexPos.y)
	{
		newColor = vec4(1);
	}
	*/

	//
	/*
	float x;
	x += v_TexPos.x * 2 * PI + newTime;
	float sinResult = ((sin(x) + 1) / 2) / v_TexPos.x;
	float thickness = 0.01f;
	if ( sinResult > v_TexPos.y && sinResult - thickness < v_TexPos.y)
	{
		newColor = vec4(1);
	}
	*/

	FragColor = newColor;
}
