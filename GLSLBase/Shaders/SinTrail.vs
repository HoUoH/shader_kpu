#version 450

in vec3 a_Position;
in vec3 a_Vel;
in vec4 a_Color;
in vec4 a_StartLifeRatioAmp;
in float a_RandValue;

uniform float u_ElapsedTime;

const vec3 c_Gravity = vec3(0, -5, 0);
const float PI = 3.141592;
const mat3 c_RP = mat3(0, -1, 0,  1, 0, 0,  0, 0, 0);

out vec4 v_Color;
out vec2 v_OriXY;
out float v_Radius;


void main()
{
	float startTime = a_StartLifeRatioAmp.x;
	float lifeTime = a_StartLifeRatioAmp.y;
	float ratio = a_StartLifeRatioAmp.z;
	float amp = a_StartLifeRatioAmp.w;
	vec3 newPos = a_Position;
	vec3 newVel = a_Vel;
	vec4 newColor = a_Color;

	v_OriXY = a_Position.xy;
	v_Radius = abs(a_Position.x);

	float spawnElapsedTime = u_ElapsedTime - startTime;	// 각 버텍스 마다 전체 흐른시간 - 첫 스폰 시작시각
	float life = lifeTime - spawnElapsedTime;

	float newAlpha = 1;

	if (spawnElapsedTime > 0) // 총 흐른시간이 첫 스폰 시각을 지났을 때
	{
		if (life < 0)
		{
			// 인자가 startTime이 아닌 이유는, startTime, lifeTime이 같은시점, 같은 길이라는 가정하에 생각해보면 편하다.
			spawnElapsedTime = mod(spawnElapsedTime, lifeTime);		
		}

		// 원 둘레를 위치 초기값으로
		newPos.x += sin(a_RandValue * 2 * PI);
		newPos.y += cos(a_RandValue * 2 * PI);

		// 원래 속도에 중력가속도를 더해준다. (V = V0 + at)
		// 설명상 넣은 라인
		//newVel = newVel + c_Gravity * spawnElapsedTime;

		// 초기 속도 없이 Only 중력으로 떨어지는
		//newPos = newPos + ( 0.5 * c_Gravity * spawnElapsedTime * spawnElapsedTime);

		// (중력 가속도 더해주기)
		// 나중 위치 = 처음 위치 + (v * t) + (1/2 * a * t^2)
		// 원래 속도에 중력가속도를 더해준다.
		// 중력이 가해지는 효과 부여
		newPos = newPos + a_Vel * spawnElapsedTime + ( 0.5 * c_Gravity * spawnElapsedTime * spawnElapsedTime);


		vec3 vSin = a_Vel * c_RP;

		// (주기 함수 따라가게 하기)
		// 중력가속도 더해준 새로운 위치에 주기 함수를 따라 가는 속도 부여
		// 중력 가해지는 효과 + 주기 함수를 따라 가는 효과
		// 주기, 진폭에 영향을 많이 받음
		// 이 라인 빼면 해당 효과 빠지는 것과 같음
		newPos = newPos + vSin * sin(spawnElapsedTime * PI * 2 * ratio) * amp;


		// 알파 블렌딩
		newAlpha = 1 - spawnElapsedTime/lifeTime;
	}
	else
	{
		gl_Position = vec4(20000, 20000, 20000, 1);
	}

	gl_Position = vec4(newPos, 1);

	// 어트리뷰트 값은 바꿀 수 없으니, 할당 한 후에 거기에 새로운 값을 넣어주자
	newColor.a = newAlpha;
	v_Color = newColor;
}
